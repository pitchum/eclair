title: Déploiement de l'application "eclair" en production
date: 2017-12-11
author: soubil
status: draft


Ce document décrit la procédure recommandée de déploiement de
l'application *eclair*.

*eclair* est a priori déployable sur n'importe quelle plateforme qui
supporte python3 et hébergeant un serveur de base de données SQL.
Ici nous allons décrire une installation sur une machine sous 
Debian GNU/Linux version 9.x (stretch) avec un serveur PostgreSQL
et un serveur HTTP nginx.


## Premier déploiement

### Pré-requis

```
sudo apt --no-install-recommends install \
wget nginx-light fcgiwrap postgresql \
uwsgi uwsgi-emperor uwsgi-plugin-python3 \
python3-virtualenv virtualenv python3-psycopg2 \
python3-pip python3-setuptools python3-pkg-resources
```

Les paquets qui suivent correspondent aux bibliothèques python utilisées
par l'applicatif. Ils ne sont pas obligatoires. Ces bibliothèques seront
installées dans le *virtualenv* quoiqu'il arrive.
Installer ces bibliothèques via les paquets plutôt que via pip
permet de profiter des mises à jour de sécurité Debian.

```
sudo apt --no-install-recommends install python3-pyramid \
python3-alembic python3-jinja2 python3-sqlalchemy-ext \
python3-paste python3-pastedeploy \
python3-bcrypt python3-pyramid python3-pyramid-jinja2 \
python-pyramid-tm python-zope.sqlalchemy \
python3-babel python3-cairocffi python3-pdfrw \
libcairo2 libpangocairo-1.0-0 libgdk-pixbuf2.0-0
```


### Préparation de postgresql

Création d'un utilisateur postgresql dédié (notez bien le mot de passe que vous aurez choisi) :

    sudo -u postgres createuser -P eclairuser

Création d'une base de données (vide) :

    sudo -u postgres createdb -O eclairuser eclair


### Création d'un utilisateur Unix

    sudo useradd --create-home --home-dir /srv/eclair --gid www-data --system eclair


### Déploiement de l'applicatif

Identifier la dernière version disponible ici :
<https://gramaton.org/~pitchum/tmp/eclair-releases/>

Et la télécharger

    sudo -s -u eclair
    cd
    wget https://gramaton.org/~pitchum/tmp/eclair-releases/eclair-${version}.tar.gz
    tar xavf eclair-${version}.tar.gz
    cd eclair
    virtualenv --system-site-packages -p python3 ~/venv
    ~/venv/bin/python setup.py install
    exit


### Configuration

On copie d'abord le fichier de configuration d'exemple :

    sudo mkdir /etc/eclair
    sudo cp /srv/eclair/eclair/production.ini /etc/eclair/eclair.conf

puis on le modifie les paramètres suivants
(sans oublier d'adapter, notamment le mot de passe postgresql) :

    sqlalchemy.url = postgresql+psycopg2://eclairuser:s3cr3t@127.0.0.1:5432/eclair
    session.cookie_signing_secret = 79887928-6640-4e81-9cc5-894a5d95f67e # saisir ici une chaine aléatoire, avec la commande uuidgen par exemple
    ...
    [alembic]
    script_location = /srv/eclair/eclair/alembic


On prépare également le dossier de logs :

    sudo mkdir /var/log/eclair
    sudo chown eclair: /var/log/eclair


### Initialisation des données

    sudo -s -u eclair
    ~/venv/bin/alembic --config /etc/eclair/eclair.conf upgrade head
    ~/venv/bin/eclair-cli -c /etc/eclair/eclair.conf db setup-default-roles
    ~/venv/bin/eclair-cli -c /etc/eclair/eclair.conf db reset-superadmin
    exit


### Lancement automatique avec uwsgi-emperor

Comme le dossier `/run` est (par défaut) un système de fichiers
temporaire il faut demander explicitement à systemd de créer le dossier
`/run/eclair` au démarrage.

Pour ce faire, créer le fichier `/usr/lib/tmpfiles.d/eclair.conf` avec
le contenu suivant:

    #Type Path          Mode UID     GID        Age Argument
    d     /run/eclair   0775 eclair  www-data   -   -

Puis lancer cette commande :

    sudo systemd-tmpfiles --create /usr/lib/tmpfiles.d/eclair.conf


Créer maintenant le fichier `/etc/eclair/eclair.wsgi` comme ceci :

```
from pyramid.paster import get_app, setup_logging
ini_path = '/etc/eclair/eclair.conf'
setup_logging(ini_path)
application = get_app(ini_path, 'main')
```


Créer ensuite le fichier `/etc/uwsgi-emperor/vassals/eclair.ini` :

```
[uwsgi]
plugins = python3
uwsgi-socket = /run/eclair/eclair.socket
chmod-socket = 775
chdir = /srv/eclair
wsgi-file=/etc/eclair/eclair.wsgi
processes = 2
threads = 2
virtualenv = /srv/eclair/venv
vacuum = True

uid = eclair
gid = www-data
```

### Configuration nginx

Créer le fichier `/etc/nginx/sites-available/eclair.conf` comme ceci :

    :::nginx
    server {
        listen 80;
        listen [::]:80;
        
        root /srv/eclair/eclair;
        location /static {
            alias /srv/eclair/eclair/eclair/static;
            expires 1h;
        }
        location / {
            uwsgi_pass unix:/run/eclair/eclair.socket;
            include uwsgi_params;
        }
    }

Puis exécuter les commandes suivantes :

    sudo rm /etc/nginx/sites-enabled/default
    sudo ln -s /etc/nginx/sites-available/eclair.conf /etc/nginx/sites-enabled/
    sudo systemctl reload nginx


## Maintenance

### Consulter les logs

### Redémarrer l'applicatif

