-- Une vue pour lister les lignes de commandes qui sont des doublons
drop view v_duplicate_orderlines;
create or replace view v_duplicate_orderlines as
	select
	count(ol.id) dups, cl.shortname client, o.refdate, p.shortname art, pc.shortname prod,
	o.id order_id, cl.id client_id, pc.id prodcenter_id, p.id product_id
	from orderline ol
	join "order" o on ol.order_id = o.id
	join workplace cl on o.client_id = cl.id
	join product p on ol.product_id = p.id
	join product_subcategory subcat on p.subcategory_name = subcat.name
	join product_category cat on subcat.category_name = cat.name
	join workplace pc on cat.production_center_id = pc.id
	group by cl.shortname, o.refdate, product_id, p.shortname, pc.shortname,
	o.id, cl.id, pc.id, p.id
	having count(ol.id) > 1

-- Affichage d'exemples de commandes avec doublons
select v.refdate date_commande, v.client, v.art, v.dups,
'https://eclair.maisonlandemaine.com/order/' ||v.client || '/' || v.refdate as client_order_url,
'https://eclair.maisonlandemaine.com/prod/' || v.prod || '/' || v.refdate as prod_plan_url
from v_duplicate_orderlines v
order by v.refdate desc, v.client, v.art


--
-- Commandes de tests (datées à partir de janvier 2042)
--

-- liste des commandes de tests restantes
select * from "order" o where o.due_date > '2040-01-01'

-- suppression des lignes de commandes de test
delete from orderline where id in (
	select ol.id
	from orderline ol
	join "order" o on ol.order_id = o.id
	where o.refdate like '2042%'
);
-- suppression des commandes de test (passées en l'an de grâce 2042)
delete from "order" o
where o.refdate like '2042%'

-- Bilan des commandes à problème
select v.refdate date, v.client, v.prod, v.art article
,'https://eclair.maisonlandemaine.com/order/' ||v.client || '/' || v.refdate as url_commande
--,'https://eclair.maisonlandemaine.com/prod/' || v.prod || '/' || v.refdate as prod_plan_url
, sum(ol_dups.quantity_ordered) qte_vue_par_la_prod
, max(ol_dups.quantity_ordered) qte_probablement_vue_par_boutique
, v.dups
, avg(ol_dups.quantity_ordered) moyenne_des_doublons
, string_agg(to_char(ol_dups.quantity_ordered, '999D9'), ' ') detail_doublons
from v_duplicate_orderlines v
--join orderline ol_single on v.order_id = ol_single.order_id and v.product_id = ol_single.product_id
join orderline ol_dups on v.order_id = ol_dups.order_id and v.product_id = ol_dups.product_id
group by v.order_id, v.product_id, url_commande, v.refdate, v.client, v.prod, v.art, v.dups
order by v.refdate desc, v.client, v.art

