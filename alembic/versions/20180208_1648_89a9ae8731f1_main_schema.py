"""Main schema

Revision ID: 89a9ae8731f1
Revises: 0ad5bd7d547d
Create Date: 2018-02-08 16:48:17.424323+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '89a9ae8731f1'
down_revision = '0ad5bd7d547d'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('appsettings',
        sa.Column('key', sa.Unicode(length=255), nullable=False),
        sa.Column('val', sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint('key', name=op.f('pk_appsettings'))
    )
    op.create_table('workplace',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('shortname', sa.Unicode(length=10), nullable=False),
        sa.Column('name', sa.Unicode(length=255), nullable=True),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('address', sa.Text(), nullable=True),
        sa.Column('lat', sa.Float(), nullable=True),
        sa.Column('lon', sa.Float(), nullable=True),
        sa.Column('warehouse', sa.Boolean(name='warehouse'), nullable=True),
        sa.Column('factory', sa.Boolean(name='factory'), nullable=True),
        sa.Column('shop', sa.Boolean(name='shop'), nullable=True),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_workplace')),
        sa.UniqueConstraint('shortname', name=op.f('uq_workplace_shortname'))
    )
    op.create_table('order',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('client_id', sa.Integer(), nullable=False),
        sa.Column('refdate', sa.Unicode(length=8), nullable=True),
        sa.Column('due_date', sa.DateTime(), nullable=False),
        sa.Column('delivery_date', sa.DateTime(), nullable=True),
        sa.Column('creationdate', sa.DateTime(), nullable=True),
        sa.Column('lastupdate', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['client_id'], ['workplace.id'], name=op.f('fk_order_client_id_workplace')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_order')),
        sa.UniqueConstraint('client_id', 'refdate', name=op.f('uq_order_client_id'))
    )
    op.create_table('product_category',
        sa.Column('name', sa.Unicode(length=50), nullable=False),
        sa.Column('description', sa.Unicode(length=255), nullable=True),
        sa.Column('vatrate', sa.Float(), nullable=True),
        sa.Column('display_color', sa.Unicode(length=7), nullable=True),
        sa.Column('production_center_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['production_center_id'], ['workplace.id'], name=op.f('fk_product_category_production_center_id_workplace')),
        sa.PrimaryKeyConstraint('name', name=op.f('pk_product_category'))
    )
    op.create_table('orderlinecustomproduct',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('quantity_ordered', sa.Float(), nullable=True),
        sa.Column('quantity_produced', sa.Float(), nullable=True),
        sa.Column('quantity_delivered', sa.Float(), nullable=True),
        sa.Column('quantity_received', sa.Float(), nullable=True),
        sa.Column('order_id', sa.Integer(), nullable=False),
        sa.Column('category_name', sa.Unicode(length=50), nullable=False),
        sa.Column('productname', sa.Unicode(length=50), nullable=True),
        sa.Column('price', sa.Float(), server_default='0', nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(['category_name'], ['product_category.name'], name=op.f('fk_orderlinecustomproduct_category_name_product_category')),
        sa.ForeignKeyConstraint(['order_id'], ['order.id'], name=op.f('fk_orderlinecustomproduct_order_id_order')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_orderlinecustomproduct'))
    )
    op.create_table('product_subcategory',
        sa.Column('name', sa.Unicode(length=50), nullable=False),
        sa.Column('category_name', sa.Unicode(length=50), nullable=True),
        sa.Column('description', sa.Unicode(length=255), nullable=True),
        sa.ForeignKeyConstraint(['category_name'], ['product_category.name'], name=op.f('fk_product_subcategory_category_name_product_category')),
        sa.PrimaryKeyConstraint('name', name=op.f('pk_product_subcategory'))
    )
    op.create_table('user_productcategory',
        sa.Column('username', sa.Unicode(length=50), nullable=False),
        sa.Column('category_name', sa.Unicode(length=50), nullable=False),
        sa.ForeignKeyConstraint(['category_name'], ['product_category.name'], name=op.f('fk_user_productcategory_category_name_product_category')),
        sa.ForeignKeyConstraint(['username'], ['user.username'], name=op.f('fk_user_productcategory_username_user'))
    )
    op.create_table('product',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('reference', sa.Unicode(length=10), nullable=False),
        sa.Column('shortname', sa.Unicode(length=50), nullable=False),
        sa.Column('longname', sa.Unicode(length=255), nullable=True),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('subcategory_name', sa.Unicode(length=50), nullable=False),
        sa.Column('creationdate', sa.DateTime(), nullable=True),
        sa.Column('lastupdate', sa.DateTime(), nullable=True),
        sa.Column('price', sa.Float(), nullable=True),
        sa.Column('packaging', sa.Unicode(length=255), nullable=True),
        sa.Column('validity_startdate', sa.DateTime(), nullable=True),
        sa.Column('validity_enddate', sa.DateTime(), nullable=True),
        sa.Column('enabled', sa.Boolean(name='enabled'), server_default='true', nullable=True),
        sa.ForeignKeyConstraint(['subcategory_name'], ['product_subcategory.name'], name=op.f('fk_product_subcategory_name_product_subcategory')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_product')),
        sa.UniqueConstraint('reference', name=op.f('uq_product_reference'))
    )
    op.create_table('orderline',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('quantity_ordered', sa.Float(), nullable=True),
        sa.Column('quantity_produced', sa.Float(), nullable=True),
        sa.Column('quantity_delivered', sa.Float(), nullable=True),
        sa.Column('quantity_received', sa.Float(), nullable=True),
        sa.Column('order_id', sa.Integer(), nullable=False),
        sa.Column('product_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['order_id'], ['order.id'], name=op.f('fk_orderline_order_id_order')),
        sa.ForeignKeyConstraint(['product_id'], ['product.id'], name=op.f('fk_orderline_product_id_product')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_orderline'))
    )
    op.create_table('product_client',
        sa.Column('product_id', sa.Integer(), nullable=False),
        sa.Column('client_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['client_id'], ['workplace.id'], name=op.f('fk_product_client_client_id_workplace')),
        sa.ForeignKeyConstraint(['product_id'], ['product.id'], name=op.f('fk_product_client_product_id_product'))
    )
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('email', sa.Unicode(length=255), nullable=True))
        batch_op.add_column(sa.Column('workplace_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key(batch_op.f('fk_user_workplace_id_workplace'), 'workplace', ['workplace_id'], ['id'])


def downgrade():
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('fk_user_workplace_id_workplace'), type_='foreignkey')
        batch_op.drop_column('workplace_id')
        batch_op.drop_column('email')

    op.drop_table('product_client')
    op.drop_table('orderline')
    op.drop_table('product')
    op.drop_table('user_productcategory')
    op.drop_table('product_subcategory')
    op.drop_table('orderlinecustomproduct')
    op.drop_table('product_category')
    op.drop_table('order')
    op.drop_table('workplace')
    op.drop_table('appsettings')
