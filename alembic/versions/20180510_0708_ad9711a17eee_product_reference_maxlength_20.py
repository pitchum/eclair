"""Product reference maxlength=20

Revision ID: ad9711a17eee
Revises: cdbae47721e4
Create Date: 2018-05-10 07:08:45.599676+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ad9711a17eee'
down_revision = 'cdbae47721e4'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('product', schema=None) as batch_op:
        batch_op.alter_column('reference',
               existing_type=sa.VARCHAR(length=10),
               type_=sa.VARCHAR(length=20))


def downgrade():
    with op.batch_alter_table('product', schema=None) as batch_op:
        batch_op.alter_column('reference',
               existing_type=sa.VARCHAR(length=20),
               type_=sa.VARCHAR(length=10))
