"""Populate default roles

Revision ID: cdbae47721e4
Revises: 89a9ae8731f1
Create Date: 2018-02-08 16:52:19.782416+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cdbae47721e4'
down_revision = '89a9ae8731f1'
branch_labels = None
depends_on = None

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session as BaseSession, relationship
Session = sessionmaker()
Base = declarative_base()


class Role(Base):
    __tablename__ = 'role'
    rolename = sa.Column(sa.Unicode(50), primary_key=True)
    description = sa.Column(sa.Text)


def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    
    roles = {rolename: Role(rolename=rolename) for rolename in ('producer', 'seller', 'admin', 'superadmin', 'carrier')}
    session.add_all(roles.values())
    
    session.commit()


def downgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    userrole = sa.table('user_role', sa.column('rolename'), sa.column('username'))
    bind.execute(userrole.delete())
    rolepermission = sa.table('role_permission', sa.column('rolename'), sa.column('permname'))
    bind.execute(rolepermission.delete())
    role = sa.table('role', sa.column('rolename'), sa.column('description'))
    bind.execute(role.delete())
    session.commit()
