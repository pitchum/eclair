"""Basic schema for authentication/authorization.

Revision ID: 0ad5bd7d547d
Revises: 
Create Date: 2017-12-08 11:39:04.694081+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0ad5bd7d547d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('role',
        sa.Column('rolename', sa.Unicode(length=50), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint('rolename', name=op.f('pk_role'))
    )
    op.create_table('user',
        sa.Column('username', sa.Unicode(length=50), nullable=False),
        sa.Column('password', sa.Unicode(length=255), nullable=True),
        sa.Column('screenname', sa.Unicode(length=255), nullable=True),
        sa.PrimaryKeyConstraint('username', name=op.f('pk_user'))
    )
    op.create_table('role_permission',
        sa.Column('rolename', sa.Unicode(length=255), nullable=False),
        sa.Column('permname', sa.Unicode(length=255), nullable=False),
        sa.ForeignKeyConstraint(['rolename'], ['role.rolename'], name=op.f('fk_role_permission_rolename_role')),
        sa.PrimaryKeyConstraint('rolename', 'permname', name=op.f('pk_role_permission'))
    )
    op.create_table('user_role',
        sa.Column('username', sa.Unicode(50), nullable=True),
        sa.Column('rolename', sa.Unicode(50), nullable=True),
        sa.ForeignKeyConstraint(['rolename'], ['role.rolename'], name=op.f('fk_user_role_rolename_role')),
        sa.ForeignKeyConstraint(['username'], ['user.username'], name=op.f('fk_user_role_username_user')),
        sa.PrimaryKeyConstraint('username', 'rolename', name=op.f('pk_user_role'))
    )


def downgrade():
    op.drop_table('user_role')
    op.drop_table('role_permission')
    op.drop_table('user')
    op.drop_table('role')
