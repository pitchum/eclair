"""Unique constraint on orderline to prevent duplicates.

Revision ID: 85dc3cedb93f
Revises: ad9711a17eee
Create Date: 2018-09-12 20:33:05.124119+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '85dc3cedb93f'
down_revision = 'ad9711a17eee'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('orderline', schema=None) as batch_op:
        batch_op.create_unique_constraint(batch_op.f('uq_orderline_order_id'), ['order_id', 'product_id'])


def downgrade():
    with op.batch_alter_table('orderline', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('uq_orderline_order_id'), type_='unique')
