import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.md')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_jinja2',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'SQLAlchemy>=1.0',
    'transaction>=1.6',
    'zope.sqlalchemy',
    'waitress',
    'alembic',
    'bcrypt>3.1.0',
    'unidecode', # needed for automatic reference generation
    'babel', # formatting dates
    'weasyprint'
    ]

tests_require = [
    'WebTest >= 1.3.1',  # py3 compat
    'pytest',  # includes virtualenv
    'pytest-cov',
    ]

setup(name='eclair',
      version='1.0.4',
      description='eclair',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Development Status :: 5 - Production/Stable",
          "Intended Audience :: End Users/Desktop",
          "Intended Audience :: Manufacturing",
          "Intended Audience :: Customer Service",
          "Natural Language :: French",
          "Programming Language :: Python",
          "Programming Language :: Python :: 3",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
          "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
      ],
      author='Florent Angebault',
      author_email='florent.angebault@soubil.com',
      url='',
      keywords='webapp mini-erp',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      extras_require={
          'testing': tests_require,
      },
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = eclair:main
      [console_scripts]
      initialize_eclair_db = eclair.scripts.initializedb:main
      eclair-cli = eclair.scripts.cli:main
      """,
      )
