import logging; log = logging.getLogger(__name__)
from sqlalchemy import (
    Table,
    Column,
    Index,
    ForeignKey,
    Integer,
    Text,
    Unicode,
    Float,
    Boolean,
    DateTime,
    UniqueConstraint,
    PrimaryKeyConstraint,
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import current_timestamp

from .meta import EclairBaseModel

import bcrypt
from datetime import datetime


lnk_product_client = Table('product_client', EclairBaseModel.metadata,
    Column('product_id', Integer, ForeignKey('product.id'), nullable=False),
    Column('client_id', Integer, ForeignKey('workplace.id'), nullable=False)
)

lnk_userrole = Table('user_role', EclairBaseModel.metadata,
    Column('username', Unicode(50), ForeignKey('user.username'), nullable=False),
    Column('rolename', Unicode(50), ForeignKey('role.rolename'), nullable=False)
)

lnk_user_productcategory = Table('user_productcategory', EclairBaseModel.metadata,
    Column('username', Unicode(50), ForeignKey('user.username'), nullable=False),
    Column('category_name', Unicode(50), ForeignKey('product_category.name'), nullable=False)
)

class User(EclairBaseModel):
    __tablename__ = 'user'
    username = Column(Unicode(50), primary_key=True, nullable=False)
    password = Column(Unicode(255))
    screenname = Column(Unicode(255))
    email = Column(Unicode(255))
    
    workplace_id = Column(Integer, ForeignKey('workplace.id'))
    workplace = relationship('Workplace')
    
    roles = relationship('Role', secondary=lnk_userrole, back_populates='users')
    
    product_categories = relationship('ProductCategory', secondary=lnk_user_productcategory, back_populates='users')
    
    def set_password(self, pw):
        pwhash = bcrypt.hashpw(pw.encode('utf8'), bcrypt.gensalt())
        self.password = pwhash.decode('utf8')
    
    def check_password(self, pw):
        if self.password is not None:
            expected_hash = self.password.encode('utf8')
            return bcrypt.checkpw(pw.encode('utf8'), expected_hash)
        return False
    
    def has_role(self, rolename):
        return rolename in (r.rolename for r in self.roles)

    def __str__(self):
        return "User {username}".format(**self.__dict__)

    def __repr__(self):
        return "User {username}".format(**self.__dict__)


class Role(EclairBaseModel):
    __tablename__ = 'role'
    rolename = Column(Unicode(50), primary_key=True)
    description = Column(Text)
    permissions = relationship('RolePermission', backref=('role'), order_by='RolePermission.permname', cascade="all, delete-orphan")
    users = relationship('User', secondary=lnk_userrole,back_populates='roles')

    def __init__(self, **kwargs):
        if 'rolename' in kwargs:
            self.rolename = kwargs.get('rolename')
    
    def __str__(self):
        return self.rolename

    def __repr__(self):
        return "Role {rolename}".format(**self.__dict__)


class RolePermission(EclairBaseModel):
    __tablename__ = 'role_permission'
    __table_args__ = (PrimaryKeyConstraint('rolename', 'permname'), )
    rolename = Column('rolename', Unicode(50), ForeignKey('role.rolename'), primary_key=True)
    permname = Column('permname', Unicode(255), primary_key=True)
    
    def __init__(self, **kwargs):
        self.rolename = kwargs['rolename']
        self.permname = kwargs['permname']

    def _to_dict(self):
        '''
        Construit une représentation Dict de l'instance.
        '''
        return {
            'rolename': self.rolename,
            'permname': self.permname,
        }
    
    def __str__(self):
        return self.permname
    
    def __repr__(self):
        return "RolePermission {rolename}-{permname}".format(**self.__dict__)


class AppSettings(EclairBaseModel):
    __tablename__ = 'appsettings'
    key = Column(Unicode(255), primary_key=True)
    val = Column(Text)


class Workplace(EclairBaseModel):
    '''
    Represents a physical location in the business network.
    It can be a shop or a production center or both.
    '''
    __tablename__ = 'workplace'
    id = Column(Integer, primary_key=True)
    shortname = Column(Unicode(10), unique=True, nullable=False)
    name = Column(Unicode(255))
    description = Column(Text)
    address = Column(Text)
    lat = Column(Float)
    lon = Column(Float)
    warehouse = Column(Boolean(name='warehouse'))
    factory = Column(Boolean(name='factory'))
    shop = Column(Boolean(name='shop'))
    
    categories = relationship('ProductCategory', back_populates="production_center")

    def roles(self):
        '''Debug function for now. List the different roles of a
        workplace based on boolean fields warehouse, factory, shop'''
        r = []
        if self.warehouse:
            r.append('warehouse')
        if self.factory:
            r.append('factory')
        if self.shop:
            r.append('shop')
        return r
    
    def legal_info(self):
        return "{}\n{}\n{}".format(self.name, self.address, self.description)
    
    def __str__(self):
        return self.shortname
    
    def __repr__(self):
        return "Workplace #{id} {shortname} {name} {description} {address} ".format(**self.__dict__)


class Product(EclairBaseModel):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    reference = Column(Unicode(20), unique=True, nullable=False)
    shortname = Column(Unicode(50), nullable=False)
    longname = Column(Unicode(255))
    description = Column(Text)
    subcategory_name = Column(Unicode(50), ForeignKey('product_subcategory.name'), nullable=False)
    subcategory = relationship('ProductSubcategory')
    creationdate = Column(DateTime, default=current_timestamp)
    lastupdate = Column(DateTime, default=current_timestamp)
    price = Column(Float)
    packaging = Column(Unicode(255))
    validity_startdate = Column(DateTime)
    validity_enddate = Column(DateTime)
    enabled = Column(Boolean(name='enabled'), default=True, server_default="true")
    
    clients = relationship('Workplace', secondary=lnk_product_client, backref='available_products', order_by='Workplace.shortname')
    
    def __init__(self, **kwargs):
        EclairBaseModel.__init__(self, **kwargs)
        _now = datetime.now()
        if 'creationdate' not in kwargs:
            self.creationdate = _now
        if 'lastupdate' not in kwargs:
            self.lastupdate = _now
    
    def has_client(self, clientname):
        return clientname in (cl.shortname for cl in self.clients)
    
    def __repr__(self):
        return "Product#{id} {shortname}".format(**self.__dict__)


class ProductSubcategory(EclairBaseModel):
    __tablename__ = 'product_subcategory'
    name = Column(Unicode(50), primary_key=True)
    category_name = Column(Unicode(50), ForeignKey('product_category.name'))
    category = relationship('ProductCategory', back_populates='subcategories')
    description = Column(Unicode(255))
    
    products = relationship('Product', back_populates="subcategory")
    
    def __repr__(self):
        return "ProductSubcategory {name}".format(**self.__dict__)


class ProductCategory(EclairBaseModel):
    __tablename__ = 'product_category'
    name = Column(Unicode(50), primary_key=True)
    subcategories = relationship('ProductSubcategory', back_populates="category", order_by='ProductSubcategory.name')
    description = Column(Unicode(255))
    vatrate = Column(Float)
    display_color = Column(Unicode(7))
    
    production_center_id = Column(Integer, ForeignKey('workplace.id'))
    production_center = relationship('Workplace')
    
    users = relationship('User', secondary=lnk_user_productcategory, back_populates='product_categories')
    
    def __str__(self):
        return "{name}".format(**self.__dict__)
    
    def __repr__(self):
        return "ProductCategory {name}".format(**self.__dict__)


class Order(EclairBaseModel):
    __tablename__ = 'order'
    __table_args__ = (UniqueConstraint('client_id', 'refdate'), )
    id = Column(Integer, primary_key=True)
    client_id = Column(Integer, ForeignKey('workplace.id'), nullable=False)
    client = relationship('Workplace')
    refdate = Column(Unicode(8)) # TODO index that column to make invoicing efficient
    due_date = Column(DateTime, nullable=False)
    delivery_date = Column(DateTime, nullable=True)
    creationdate = Column(DateTime, default=current_timestamp)
    lastupdate = Column(DateTime, default=current_timestamp)
   
    lines = relationship('OrderLine', back_populates='order')
    custom_lines = relationship('OrderLineCustomProduct', back_populates='order')
    
    def __init__(self, **kwargs):
        _now = datetime.now()
        self.delivery_date = None
        self.due_date = None
        self.creationdate = _now
        self.lastupdate = _now
    
    def __repr__(self):
        return "Order {refdate} by {client_id}".format(**self.__dict__)


class OrderLine(EclairBaseModel):
    __tablename__ = 'orderline'
    __table_args__ = (UniqueConstraint('order_id', 'product_id'), )
    id = Column(Integer, primary_key=True)
    quantity_ordered   = Column(Float)
    quantity_produced  = Column(Float)
    quantity_delivered = Column(Float)
    quantity_received  = Column(Float)
    order_id = Column(Integer, ForeignKey('order.id'), nullable=False)
    order = relationship('Order', back_populates='lines')
    product_id = Column(Integer, ForeignKey('product.id'), nullable=False)
    product = relationship('Product')
    
    
    def __init__(self, **kwargs):
        if 'quantity' in kwargs:
            self.quantity = kwargs.get('quantity')
        if 'order_id' in kwargs:
            self.order_id = kwargs.get('order_id')
        if 'product_id' in kwargs:
            self.product_id = kwargs.get('product_id')
    
    def __str__(self):
        return "Product#{} qty: {}".format(self.product_id, self.quantity_ordered)
    
    def __repr__(self):
        return "OrderLine {quantity_ordered} {product_id}".format(**self.__dict__)


class OrderLineCustomProduct(EclairBaseModel):
    __tablename__ = 'orderlinecustomproduct'
    id = Column(Integer, primary_key=True)
    quantity_ordered   = Column(Float)
    quantity_produced  = Column(Float)
    quantity_delivered = Column(Float)
    quantity_received  = Column(Float)
    
    order_id = Column(Integer, ForeignKey('order.id'), nullable=False)
    order = relationship('Order', back_populates='custom_lines')
    
    category_name = Column(Unicode(50), ForeignKey('product_category.name'), nullable=False)
    category = relationship('ProductCategory')
    
    productname = Column(Unicode(50))
    price = Column(Float, nullable=False, server_default="0") # this is the price that the final customer will pay
    description = Column(Text)
    
    def reference(self):
        return "SPE{:04d}".format(self.id)
    
    def display_label(self):
        return "{} {:.2f}€ (spécial)".format(self.productname, self.price)
    
    def __str__(self):
        return "OrderLineCustomProduct {productname} qty: {quantity_ordered}".format(**self.__dict__)
    
    def __repr__(self):
        return "OrderLineCustomProduct#{id} {quantity_ordered} ".format(**self.__dict__)
