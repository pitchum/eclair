import logging; log = logging.getLogger(__name__)
from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPForbidden

from datetime import datetime, timedelta
from collections import OrderedDict
from ..models import User, Order, OrderLine, Product, Workplace, ProductSubcategory, ProductCategory
from eclair import util
from eclair.util import dbapi, EclairConfigManager

from sqlalchemy.orm import joinedload, aliased
from sqlalchemy import func


@view_config(route_name='production_plan', renderer='../templates/production_plan.jinja2', permission='order:view')
@view_config(route_name='production_plan', renderer='json', xhr=True, permission='order:view')
def todolist(request):
    '''
    Matchdict contains ``factoryname`` and ``refdate``.
    ``refdate`` is either a date in a compact format ('20170423') or
    a keyword like 'today' or 'tomorrow'.
    '''
    factoryname = request.matchdict.get('factoryname')
    prev_refdate, refdate, next_refdate = util.refdates(request.matchdict.get('refdate'))
    if request.params.get('allcats', 'false') == 'true':
        product_categorynames = None
    else:
        product_categorynames = [cat for cat in request.userdata.get('product_categorynames')]
    clientnames, lines = dbapi.fetch_prod_todolist(factoryname, refdate=refdate, product_categorynames=product_categorynames)
    customproducts = dbapi.custom_products_for_production_center(factoryname, refdate, clientnames)
    
    custom_products_planning = dbapi._list_custom_products_planning(factoryname, refdate)
    
    # Should the page be auto-reloaded?
    now = datetime.now()
    config_manager = EclairConfigManager(request.dbsession)
    orderlockdate = config_manager.get_orderlockdatetime(refdate)
    lock_delay = int((orderlockdate - now).total_seconds())
    
    return {
        "orderlines": lines,
        "refdate": refdate,
        "prev_refdate": prev_refdate,
        "next_refdate": next_refdate,
        "factoryname": factoryname,
        "clients": clientnames,
        "custom_products_planning": custom_products_planning,
        "customproducts": customproducts,
        "lock_delay": lock_delay,
        "now": now,
        "orderlockdate": orderlockdate,
    }


@view_config(route_name='production_plans', renderer='../templates/production_plans.jinja2', permission='order:view')
def prod_plans(request):
    factoryname = request.matchdict.get('factoryname')
    refdate = request.matchdict.get('refdate')
    daycount = request.params.get('days', 7)
    refdates, lines = dbapi.fetch_prod_todolists_by_dates(factoryname, refdate, daycount)
    return {
        "refdates": refdates,
        "orderlines": lines,
        "factoryname": factoryname,
    }
