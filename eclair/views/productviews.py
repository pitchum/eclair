from pyramid.response import Response
from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from datetime import datetime
import re

from sqlalchemy.orm import joinedload

import logging; logging.basicConfig()
log = logging.getLogger(__name__)

from ..models import User, Product, ProductSubcategory, ProductCategory, Workplace
from eclair import util

from .default import BaseView, register_views


@view_config(route_name='products.byfactory', renderer='../templates/products.jinja2')
@view_config(route_name='products', renderer='../templates/products.jinja2')
def list_products(request):
    q = request.dbsession.query(Product)\
        .options(joinedload(Product.subcategory).joinedload(ProductSubcategory.category))\
        .options(joinedload(Product.clients))\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)
    if 'factoryname' in request.matchdict:
        wpshortname = request.matchdict.get('factoryname')
        q = q.join(ProductCategory.production_center)\
            .filter(Workplace.shortname == wpshortname)
        request.session['catalogue_filter_factoryname'] = wpshortname
    elif 'catalogue_filter_factoryname' in request.session:
        request.session.pop('catalogue_filter_factoryname')
    q = q.order_by(ProductSubcategory.category_name, Product.subcategory_name, Product.shortname)
    products = q.all()
    prod_centers = request.dbsession.query(Workplace)\
        .filter(Workplace.factory)\
        .order_by(Workplace.shortname)\
        .all()
    return {
        "products": products,
        "production_centers": prod_centers,
    }


@view_config(route_name='product', renderer='../templates/product_edit.jinja2', permission='product:modify')
def product_edit(request):
    ref = request.matchdict.get('reference')
    product = request.dbsession.query(Product).filter(Product.reference == ref).first()
    possible_clients = request.dbsession.query(Workplace)\
            .filter(Workplace.shop)\
            .order_by(Workplace.shortname)\
            .all()
    subcategories = request.dbsession.query(ProductSubcategory)\
        .options(joinedload(ProductSubcategory.category).joinedload(ProductCategory.production_center))\
        .join(ProductSubcategory.category)\
        .filter(Workplace.factory == True)\
        .order_by(Workplace.shortname, ProductCategory.name, ProductSubcategory.name)\
        .all()
    return {
        "product": product,
        "subcategories": subcategories,
        "possible_clients": possible_clients,
    }


@view_config(route_name='product', renderer='json', request_method='POST', permission='product:modify')
def product_update(request):
    errors = []
    ref = request.matchdict.get('reference')
    data = dict(request.params)
    if 'clients' in data:
        del data['clients'] # clients need special treatment, cf. a few lines below
    product = request.dbsession.query(Product).filter(Product.reference == ref).first()
    
    if request.matchdict.get('reference', 'new') == 'new':
        product = Product(**data)
        # XXX dirty hack start
        if data.get('validity_startdate'):
            product.validity_startdate = datetime.strptime(data.get('validity_startdate'), '%Y-%m-%d')
        else:
            product.validity_startdate = None
        if data.get('validity_enddate'):
            product.validity_enddate = datetime.strptime(data.get('validity_enddate'), '%Y-%m-%d')
        else:
            product.validity_enddate = None
        product.price = product.price or None
        # XXX dirty hack end
        # check if the new reference is valid and unique
        reference = data.get('reference', '')
        if not reference.strip():
            errors.append("La référence saisie n'est pas valide.")
        else:
            existing_ref = request.dbsession.query(Product).filter(Product.reference == reference).first()
            if existing_ref is not None:
                errors.append("La référence {} existe déjà.".format(reference))

    product.reference = data.get('reference', product.reference)
    product.shortname = data.get('shortname', product.shortname)
    product.longname = product.shortname
    product.packaging = data.get('packaging', product.packaging)
    product.enabled = 'enabled' in data
    if data.get('price'): # TODO check that price is a valid float
        price = data.get('price', product.price)
        price.replace(',', '.') # XXX really bad thing TODO FIXME
        product.price = price
    if data.get('validity_startdate') and not data.get('validity_enddate'):
        errors.append('Vous avez spécifié une date de début mais pas de date de fin.')
    if not data.get('validity_startdate') and data.get('validity_enddate'):
        errors.append('Vous avez spécifié une date de fin mais pas de date de début.')
    elif data.get('validity_startdate') and data.get('validity_enddate'):
        dt1 = data.get('validity_startdate')
        dt2 = data.get('validity_enddate')
        if dt1:
            dt1 = datetime.strptime(dt1, '%Y-%m-%d')
        if dt2:
            dt2 = datetime.strptime(dt2, '%Y-%m-%d')
        if dt1 and dt2:
            # check that validity dates are correct (start is before end)
            if dt1 > dt2:
                errors.append('Dates de début et de fin incorrectes : {} > {}'.format(dt1.strftime('%Y-%m-%d'), dt2.strftime('%Y-%m-%d')))
                dt1 = None
                dt2 = None
        if dt1:
            product.validity_startdate = dt1
        if dt2:
            dt2 = dt2.replace(hour=23, minute=59, second=59)
            product.validity_enddate = dt2
    # update associated clients
    clientnames = request.params.getall('clients')
    clients = request.dbsession.query(Workplace).filter(Workplace.shortname.in_(clientnames)).all()
    product.clients = clients
    product.subcategory_name = data.get('subcategory_name', product.subcategory_name)
    if not errors:
        request.dbsession.merge(product)
        if 'catalogue_filter_factoryname' in request.session:
            factoryname = request.session.get('catalogue_filter_factoryname')
            return HTTPFound(location=request.route_path('products.byfactory', factoryname=factoryname))
        else:
            return HTTPFound(location=request.route_path('products'))
    else:
        msg = "L'enregistrement n'a pas pu être effectué.<br/>"
        for error in errors:
            msg += error + "<br/>"
        request.session.flash(msg)
        # Restore page with user's input
        request.override_renderer = '../templates/product_edit.jinja2'
        subcategories = request.dbsession.query(ProductSubcategory)\
            .autoflush(False)\
            .options(joinedload(ProductSubcategory.category).joinedload(ProductCategory.production_center))\
            .join(ProductSubcategory.category)\
            .filter(Workplace.factory == True)\
            .order_by(Workplace.shortname, ProductCategory.name, ProductSubcategory.name)\
            .all()
        possible_clients = request.dbsession.query(Workplace)\
            .autoflush(False)\
            .filter(Workplace.shop)\
            .order_by(Workplace.shortname)\
            .all()
        return {
            "product": product,
            "subcategories": subcategories,
            "possible_clients": possible_clients,
        }
        request.dbsession.expunge(product)


@view_config(route_name='product_categories', renderer='../templates/product_categories_list.jinja2')
def categories_list(request):
    categories = request.dbsession.query(ProductCategory)\
        .options(joinedload(ProductCategory.subcategories))\
        .options(joinedload(ProductCategory.users))\
        .order_by(ProductCategory.name)\
        .all()
    return {
        "categories": categories,
    }


@view_config(route_name='product_category', renderer='../templates/product_category_edit.jinja2', permission='product:modify')
def category_edit(request):
    categoryname = request.matchdict.get('categoryname')
    category = request.dbsession.query(ProductCategory)\
        .options(
            joinedload(ProductCategory.subcategories)
            .joinedload(ProductSubcategory.products)
        )\
        .options(joinedload(ProductCategory.users))\
        .get(categoryname)
    available_users = request.dbsession.query(User)\
        .join(User.workplace)\
        .filter(Workplace.factory == True)
    available_production_centers = request.dbsession.query(Workplace)\
        .filter(Workplace.factory == True)\
        .order_by(Workplace.shortname)
    return {
        "category": category,
        "available_users": available_users,
        "available_production_centers": available_production_centers,
    }


@view_config(route_name='product_category', renderer='json', request_method='POST', permission='product:modify')
def category_update(request):
    categoryname = request.matchdict.get('categoryname')
    if request.matchdict.get('categoryname') == 'new':
        product_category = ProductCategory()
        request.dbsession.add(product_category)
    else:
        product_category = request.dbsession.query(ProductCategory).get(categoryname)
    product_category.name = request.params.get('name')
    product_category.description = request.params.get('description')
    product_category.production_center_id = request.params.get('production_center_id', product_category.production_center_id)
    product_category.vatrate = request.params.get('vatrate', product_category.vatrate)
    product_category.display_color = request.params.get('display_color', product_category.display_color)
    request.dbsession.flush()
    request.dbsession.refresh(product_category)
    return HTTPFound(location=request.route_path('product_category', categoryname=product_category.name))


@view_config(route_name='product_category', renderer='json', request_method='PUT', permission='product:modify')
def category_update_details(request):
    categoryname = request.matchdict.get('categoryname')
    data = request.json_body
    product_category = request.dbsession.query(ProductCategory)\
        .options(joinedload(ProductCategory.users))\
        .get(categoryname)
    errors = []
    
    # Attach existing users to the product category
    if 'add_users' in data:
        usernames = data.get('add_users')
        current_usernames = [u.username for u in product_category.users]
        users = request.dbsession.query(User)\
            .options(joinedload(User.workplace))\
            .filter(User.username.in_(usernames))\
            .all()
        for user in users:
            if user.username in current_usernames:
                erros.append("{} est déjà réponsable de la famille {}".format(user.username, product_category.name))
                continue
            if user.username not in usernames:
                log.warning("Weird, how could it happen? User %s could not be added to category %s.", user.username, categoryname)
                continue
            if not user.workplace.factory:
                errors.append("L'utilisateur {} n'est pas associé à un centre de production.".format(user.username))
                continue
            product_category.users.append(user)
            usernames.remove(user.username)
        if usernames:
            for u in usernames:
                errors.append("L'utilisateur {} n'existe pas.".format(u))
        request.dbsession.flush()
    
    # Detach users to from the product category
    if 'remove_users' in data:
        usernames = data.get('remove_users')
        users = request.dbsession.query(User)\
            .options(joinedload(User.workplace))\
            .filter(User.username.in_(usernames))\
            .all()
        for user in users:
            product_category.users.remove(user)
        request.dbsession.flush()
    
    regex_valid_name = re.compile('^[A-Za-z][A-Za-z0-9_-]+$')
    # Create new subcategories for the product category
    if 'create_subcategories' in data:
        subcategorynames = data.get('create_subcategories')
        for subcatname in subcategorynames:
            if not regex_valid_name.match(subcatname):
                errors.append("Nom invalide : '{}'.\nSaisissez un nom sans espace commançant par une lettre. La suite peut comporter des chiffres et des tirets.".format(subcatname))
        # Check existing name
        existing_subcats = request.dbsession.query(ProductSubcategory)\
            .filter(ProductSubcategory.name.in_(subcategorynames))
        for existing_subcat in existing_subcats:
            errors.append("Il existe déjà une sous-famille nommée {} (dans la famille {}).".format(existing_subcat.name, existing_subcat.category_name))
            subcategorynames.remove(existing_subcat.name)
        # Insert new valid names
        for subcatname in subcategorynames:
            subcat = ProductSubcategory()
            subcat.category_name = categoryname
            subcat.name = subcatname
            request.dbsession.add(subcat)
    
    if not errors:
        return {"success": True}
    return {
        "success": False,
        "errors": errors,
    }


@view_config(route_name='product_subcategory', renderer='../templates/product_subcategory_edit.jinja2', permission='product:modify')
def subcategory_edit(request):
    subcategoryname = request.matchdict.get('subcategoryname')
    subcategory = request.dbsession.query(ProductSubcategory)\
        .options(joinedload(ProductSubcategory.category))\
        .options(joinedload(ProductSubcategory.products))\
        .get(subcategoryname)
    return {
        "subcategory": subcategory,
    }


@view_config(route_name='product.generate_ref', renderer='json', request_method='POST', xhr=True)
def generate_reference(request):
    productname = request.json_body.get("productname")
    subcatname = request.json_body.get("subcatname")
    factoryname = request.json_body.get("factoryname")
    res = util._generate_reference(factoryname, subcatname, productname)
    # if reference already exist append an autoincremented number
    existing = request.dbsession.query(Product.reference).filter(Product.reference == res).first()
    if existing:
        similar = set([row.reference for row in request.dbsession.query(Product.reference).filter(Product.reference.like(res + '%'))])
        for i in range(1, 99):
            candidate = res + str(i)
            if candidate not in similar:
                res = candidate
                break
    return {
        "candidates": [res],
    }

## Attempt to auto-generate a simple REST API
## Cf. http://codelike.com/blog/2014/04/27/a-rest-api-with-python-and-pyramid/
## Maybe it was a mistake. To be continued...
# TODO squash useless code
@register_views(route='product', collection_route='products')
class ProductView(BaseView):
    item_cls = Product
    #  schema_cls = PostSchema
    
    def create_item(self):
        _now = datetime.now()
        data = self.request.json_body
        if 'creationdate' not in data:
            data['creationdate'] = _now
        if 'lastupdate' not in data:
            data['lastupdate'] = _now
        item = self.item_cls(**data)
        self.request.dbsession.add(item)
        self.request.dbsession.flush()
        return item
