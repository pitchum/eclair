import logging; log = logging.getLogger(__name__)
from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPForbidden

from datetime import datetime, timedelta
from collections import OrderedDict
from ..models import Order, OrderLine, Product, Workplace, ProductSubcategory, ProductCategory
from eclair import util
from eclair.util import dbapi


@view_config(route_name='delivery', renderer='../templates/delivery_view.jinja2', permission='order:view')
def delivery_view(request):
    prev_refdate, refdate, next_refdate = util.refdates(request.matchdict.get('refdate'))
    factoryname = request.matchdict.get('factoryname')
    
    fmt = request.params.get('fmt', 'table')
    if fmt == 'table':
        clients, lines = dbapi.fetch_delivery_plan(factoryname, refdate)
        return {
            "clients": clients,
            "orderlines": lines,
            "refdate": refdate,
            "prev_refdate": prev_refdate,
            "next_refdate": next_refdate,
            "factoryname": factoryname,
        }
    else:
        items_by_destination = dbapi.fetch_delivery_plan_simplified(factoryname, refdate)
        return {
            "refdate": refdate,
            "prev_refdate": prev_refdate,
            "next_refdate": next_refdate,
            "factoryname": factoryname,
            "items_by_destination": items_by_destination,
        }
