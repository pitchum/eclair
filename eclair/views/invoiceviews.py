from pyramid.response import Response
from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from datetime import datetime

import logging; logging.basicConfig()
log = logging.getLogger(__name__)

from io import BytesIO

from weasyprint import HTML

from pyramid.renderers import render

from eclair.util import dbapi
from eclair import util
from eclair.models import allmodels as mdl


@view_config(route_name='invoice', renderer='../templates/invoice.jinja2')
def invoice_pdf(request):
    factoryname = request.matchdict.get('factoryname')
    clientname = request.matchdict.get('clientname')
    invoice_data = dbapi.fetch_invoice_data(factoryname, clientname, refmonth=request.matchdict.get('refmonth'))
    if request.params.get('format', 'html') == 'pdf':
        html = render('templates/invoice_pdf.jinja2', {"invoice": invoice_data, "now": datetime.now()}, request)
        html = HTML(string=html)
        result = html.write_pdf()
        return Response(result, content_type='application/pdf', content_disposition='attachment; filename={}.pdf'.format(invoice_data.get('reference', 'invoice')))
    else:
        invoice_data['factoryname'] = factoryname
        invoice_data['clientname'] = clientname
        return invoice_data


@view_config(route_name='invoices', renderer='../templates/invoices_list_for_production_center.jinja2')
def invoice_list_for_production_center(request):
    refmonths = util.refmonth_range('current', before=3, after=0)
    clients = request.dbsession.query(mdl.Workplace)\
        .filter(mdl.Workplace.shop == True)\
        .order_by(mdl.Workplace.shortname)\
        .all()
    return {
        "refmonths": refmonths,
        "clients": clients,
        "factoryname": request.matchdict.get('factoryname'),
    }
