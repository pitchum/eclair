from pyramid.response import Response
from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from datetime import datetime

import logging; logging.basicConfig()
log = logging.getLogger(__name__)

from ..models import Workplace
from sqlalchemy.orm import joinedload


@view_config(route_name='workplaces', renderer='json', xhr=True)
@view_config(route_name='workplaces', renderer='../templates/workplaces.jinja2')
def workplaces_list(request):
    workplaces = request.dbsession.query(Workplace).order_by(Workplace.shortname).all()
    return {
        "workplaces": workplaces,
    }


@view_config(route_name='workplace', renderer='json', xhr=True)
@view_config(route_name='workplace', renderer='../templates/workplace_edit.jinja2')
def workplace_edit(request):
    shortname = request.matchdict.get('shortname')
    workplace = request.dbsession.query(Workplace)\
        .options(joinedload(Workplace.categories))\
        .filter(Workplace.shortname == shortname).first()
    return {
        "workplace": workplace,
        "possible_roles": ["shop", "factory"],
    }


@view_config(route_name='workplaces', renderer='json', request_method='POST', permission='workplace:modify')
def workplace_create(request):
    data = request.json_body
    shortname = data.get('shortname')
    existing = request.dbsession.query(Workplace).filter(Workplace.shortname == shortname).first()
    if existing:
        return {
            "success": False,
            "error": "Workplacename {} already exist.".format(shortname),
        }
    workplace = Workplace()
    workplace.shortname = shortname
    workplace.name = ''
    workplace.address = ''
    request.dbsession.add(workplace)
    return {
        "success": True,
        "workplace": workplace,
    }


@view_config(route_name='workplace', renderer='json', request_method='POST', permission='workplace:modify')
def workplace_update(request):
    shortname = request.matchdict.get('shortname')
    workplace = request.dbsession.query(Workplace).filter(Workplace.shortname == shortname).first()
    data = request.params
    workplace.shortname = data.get('shortname', workplace.shortname)
    workplace.address = data.get('address', workplace.address)
    workplace.description = data.get('description', workplace.description)
    workplace.name = data.get('name', workplace.name)
    rolenames = data.getall('roles')
    workplace.warehouse = 'warehouse' in rolenames
    workplace.factory = 'factory' in rolenames
    workplace.shop = 'shop' in rolenames
    request.dbsession.merge(workplace)
    return HTTPFound(location=request.route_path('workplaces'))
