from pyramid.response import Response
from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from datetime import datetime

from sqlalchemy.orm import joinedload

import logging; logging.basicConfig()
log = logging.getLogger(__name__)

from eclair.util import EclairConfigManager


@view_config(route_name='config', renderer='../templates/appconfig.jinja2', permission='config:view')
def config_view(request):
    config_manager = EclairConfigManager(request.dbsession)
    return {
        "config": config_manager.asdict(),
    }


@view_config(route_name='config', renderer='json', request_method='POST', permission='config:modify')
def config_update(request):
    # order-lock time
    form_data = request.params
    config_manager = EclairConfigManager(request.dbsession)
    orderlocktime = form_data.get('orderlocktime', '00:00')
    internal_price_conversion_ratio = form_data.get('internal_price_conversion_ratio', '55')
    errors = []
    try:
        orderlocktime = config_manager.update_orderlocktime(orderlocktime)
    except Exception as e:
        errors.append(str(e))
    config_manager.set('internal_price_conversion_ratio', internal_price_conversion_ratio)
    
    if not errors:
        return HTTPFound(location=request.route_path('dashboard'))
    else:
        msg = "L'enregistrement n'a pas pu être effectué.<br/>"
        for error in errors:
            msg += error + "<br/>"
        request.session.flash(msg)
        return HTTPFound(location=request.route_path('config'))
