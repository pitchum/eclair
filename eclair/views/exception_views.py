import logging; log = logging.getLogger(__name__)
from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPForbidden, HTTPError
from pyramid.security import authenticated_userid, NO_PERMISSION_REQUIRED

from ..models import allmodels as mdl


@forbidden_view_config(renderer='../templates/forbidden.jinja2', xhr=False)
@forbidden_view_config(renderer='json', xhr=True)
def forbidden(context, request):
    '''2 possible cases:
        - the user is authenticated but has insufficient permissions => give explicit error message
        - the user is not authenticated => redirect to login page
    '''
    request.response.status = context.status
    if authenticated_userid(request):
        # the user is authenticated but has insufficient permissions => give explicit error message
        route_name = request.matched_route.name
        possible_roles = [r[0] for r in request.dbsession.query(mdl.Role.rolename)\
            .select_from(mdl.RolePermission)\
            .join(mdl.Role.permissions)\
            .filter(mdl.RolePermission.permname == context.result.permission)\
            .all()]
        if request.is_xhr:
            return {
                'error': "L'accès à cette fonctionnalité (%s) nécessite la permission '%s'." % (route_name, context.result.permission),
            }
        else:
            return {
                "route_name": route_name,
                "required_permission": context.result.permission,
                "possible_roles": possible_roles,
            }
    else:
        # the user is not authenticated => redirect to login page
        request.session.flash("Votre session a expiré. \nVeuillez vous authentifier à nouveau.")
        if request.is_xhr:
            return {
                "redirect": request.route_path('login'),
            }
        else:
            request.session['next'] = request.path
            return HTTPFound(location=request.route_path('login'))


@view_config(context=Exception, permission=NO_PERMISSION_REQUIRED)
def generic_exception_view(exc, request):
    log.error(request.path, exc_info=True)
    response = Response(
'''500 Internal Server Error

Exception: %s
%s

''' % (exc.__class__.__name__, str(exc)))
    response.content_type = 'text/plain'
    response.status = "500 %s" % exc.__class__.__name__
    return response


@view_config(context=HTTPError, xhr=False, permission=NO_PERMISSION_REQUIRED)
def generic_httperror_view(exc, request):
    response = Response(
'''%s

Exception: %s
%s

''' % (exc.status, exc.__class__.__name__, exc.detail))
    response.content_type = 'text/plain'
    response.status = exc.status
    return response


@view_config(context=HTTPError, xhr=True, renderer='json', permission=NO_PERMISSION_REQUIRED)
def generic_httperror_xhr_view(exc, request):
    request.response.status = '%s' % (exc.status)
    return {
        "exception": str(exc),
    }
