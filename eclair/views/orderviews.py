import logging; log = logging.getLogger(__name__)
from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPForbidden
from pyramid.threadlocal import get_current_request

from datetime import datetime, timedelta
from collections import OrderedDict
from ..models import Order, OrderLine, OrderLineCustomProduct, Product, Workplace, ProductCategory, ProductSubcategory

from eclair import util
from eclair.util import dbapi, EclairConfigManager


from sqlalchemy.orm import joinedload, aliased
import sqlalchemy as sa


@view_config(route_name='orders', renderer='../templates/order_week.jinja2', permission='order:modify')
def order_week(request):
    clientname = request.matchdict.get('clientname')
    refdate = request.matchdict.get('refdate')
    
    startdate = util.parse_refdate(refdate)
    startdate = startdate + timedelta(days=-7)
    refdates = util.refdates_range(startdate, 10)
    
    # Generate all refdates for the week
    enddate = util.parse_refdate(refdates[-1])
    
    # Navigation buttons need refdates
    prev_week = util.normalize_refdate(util.parse_refdate(refdate) + timedelta(days=-7))
    next_week = util.normalize_refdate(util.parse_refdate(refdate) + timedelta(days=7))
    prev_refdate = util.normalize_refdate(util.parse_refdate(refdate) + timedelta(days=-1))
    next_refdate = util.normalize_refdate(util.parse_refdate(refdate) + timedelta(days=1))
    
    # Client info
    client = request.dbsession.query(Workplace)\
        .filter(Workplace.shortname == clientname).first()
    
    # Products available for the client
    products = util.dbapi.fetch_products_indexed_by_ref(clientnames=[clientname])
    products_by_id = {p.id:p for p in products.values()}
    
    # TODO identify products with validity dates conflicting with the current date range
    
    # TODO include orderline custom products if any
    
    # Fetch existing order lines
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    q_orders = request.dbsession.query(Order)\
        .join(client_alias, Order.client)\
        .filter(client_alias.shortname == clientname)\
        .filter(Order.refdate.in_(refdates))
    items_by_ref_and_date = {}
    for order in q_orders:
        if order.refdate not in items_by_ref_and_date:
            items_by_ref_and_date[order.refdate] = {}
        for line in order.lines:
            product = products_by_id.get(line.product_id)
            if not product:
                log.error("Product present in order is not in products allowed for customer. Customer: %s, Product #%s", clientname, line.product.reference + " " + line.product.shortname)
                continue
            items_by_ref_and_date[order.refdate][product.reference] = line
    
    # Determine which refdates are before lockdate and should therefore be made readonly
    config_manager = EclairConfigManager(request.dbsession)
    
    # Combine all data in a matrix suitable for rendering
    matrix = OrderedDict()
    for ref,p in products.items():
        line = {
            "product": p,
        }
        for refdate in refdates:
            if refdate in items_by_ref_and_date and ref in items_by_ref_and_date[refdate]:
                quantity_ordered = items_by_ref_and_date[refdate][ref].quantity_ordered
            else:
                quantity_ordered = 0
            if config_manager.get_orderlockdatetime(refdate) > datetime.now():
                # TODO also check product's validity date range
                editable = True
            else:
                editable = False
            line[refdate] = {
                "editable": editable,
                "quantity_ordered": quantity_ordered,
            }
        matrix[ref] = line
    
    return {
        "clientname": clientname,
        "startdate": startdate,
        "enddate": enddate,
        "refdate": refdate,
        "refdates": refdates,
        "products": products.values(),
        "prev_refdate": prev_refdate,
        "next_refdate": next_refdate,
        "prev_week": prev_week,
        "next_week": next_week,
        "lines": matrix,
    }


@view_config(route_name='order', renderer='../templates/order_edit.jinja2', permission='order:modify')
def order_edit(request):
    '''
    Matchdict contains ``clientname`` and ``refdate``.
    ``refdate`` is either a date in a compact format ('20170423') or
    a keyword like 'today' or 'tomorrow'.
    '''
    clientname = request.matchdict.get('clientname')
    prev_refdate, refdate, next_refdate = util.refdates(request.matchdict.get('refdate'))
    
    # Order page is readonly when is too late
    order_readonly = False
    config_manager = EclairConfigManager(request.dbsession)
    orderlockdate = config_manager.get_orderlockdatetime(refdate)
    if orderlockdate < datetime.now():
        order_readonly = True
    # Superadmin is always allowed to edit any order
    if order_readonly and request.params.get('edit') == 'true' and 'superadmin' in request.userdata.get('rolenames'):
        order_readonly = False
    
    order, items, total_price = dbapi.fetch_order_data(clientname, refdate)
    
    # Custom product lines
    customproducts = request.dbsession.query(OrderLineCustomProduct)\
        .filter(OrderLineCustomProduct.order_id == order.id)
    
    if order_readonly:
        request.override_renderer = '../templates/order_display.jinja2'
    return {
        "clientname": clientname,
        "order": order,
        "refdate": refdate,
        "prev_refdate": prev_refdate,
        "next_refdate": next_refdate,
        "products": items,
        "customproducts": customproducts,
        "total_price": total_price,
        "today": datetime.now(),
    }


@view_config(route_name='orderlines', renderer='json', request_method='POST', permission='order:modify')
def oderlines_update(request):
    '''Update multiple items of an order'''
    clientname = request.matchdict.get('clientname')
    refdate = request.matchdict.get('refdate')
    order = dbapi.get_order_and_create_it_if_needed(clientname, util.normalize_refdate(refdate))
    data = request.json_body
    res = {}
    product_ids_to_update = [int(k) for k in data.keys()]
    
    # Update existing orderlines first
    existing_orderlines = request.dbsession.query(OrderLine)\
            .join(OrderLine.order)\
            .join(Order.client)\
            .filter(Workplace.shortname == clientname)\
            .filter(Order.refdate == refdate)\
            .filter(OrderLine.product_id.in_(product_ids_to_update))
    for existing_orderline in existing_orderlines:
        product_ids_to_update.remove(existing_orderline.product_id)
        qty = data[str(existing_orderline.product_id)].get('quantity_ordered') or '0'
        existing_orderline.quantity_ordered = max(0, float(qty))
        request.dbsession.merge(existing_orderline)
        # log.debug("Order {}-{}, updated orderline: quantity = {} for product #{}".format(clientname, order.refdate, existing_orderline.quantity_ordered, existing_orderline.product_id))
        res[existing_orderline.product_id] = existing_orderline.quantity_ordered
    
    # Remaining product ids are new orderlines
    # XXX seems to be possible to insert duplicates here... let's try using UPSERT
    for pid in product_ids_to_update:
        qty = data[str(pid)].get('quantity_ordered') or '0'
        orderline = OrderLine(product_id=pid)
        orderline.order_id = order.id
        orderline.quantity_ordered = max(0, float(qty))
        request.dbsession.add(orderline)
        # log.debug("Order {}-{}, inserted orderline: quantity = {} for product #{}".format(clientname, order.refdate, orderline.quantity_ordered, orderline.product_id))
        res[pid] = orderline.quantity_ordered
    return {
        "status": "success",
        "updated_orderlines": res,
    }


@view_config(route_name='orderlinecustomproduct.add', renderer='../templates/order_add_custom_line.jinja2', permission='order:modify')
@view_config(route_name='orderlinecustomproduct', renderer='../templates/order_add_custom_line.jinja2', permission='order:view')
def orderlinecustomproduct_edit(request):
    if 'id' in request.matchdict:
        orderlinecustomproduct = request.dbsession.query(OrderLineCustomProduct)\
            .options(
                joinedload(OrderLineCustomProduct.order, innerjoin=True)
                .joinedload(Order.client)
            )\
            .get(request.matchdict.get('id'))
        clientname = orderlinecustomproduct.order.client.shortname
        refdate = orderlinecustomproduct.order.refdate
        user_can_edit = request.userdata.get('workplace_shortname') == orderlinecustomproduct.order.client.shortname \
            and 'seller' in request.userdata.get('rolenames')
        if request.params.get('edit') == 'true' and 'superadmin' in request.userdata.get('rolenames'):
            user_can_edit = True
        if not user_can_edit:
            request.override_renderer = '../templates/orderline_custom_product_view.jinja2'
    else:
        orderlinecustomproduct = OrderLineCustomProduct()
        clientname = request.matchdict.get('clientname')
        refdate = request.matchdict.get('refdate')
    categories = request.dbsession.query(ProductCategory).all()
    return {
        "clientname": clientname,
        "refdate": refdate,
        "categories": categories,
        "orderlinecustomproduct": orderlinecustomproduct,
    }


@view_config(route_name='orderlinecustomproduct', renderer='json', request_method='POST', permission='order:modify')
@view_config(route_name='orderlinecustomproducts', renderer='json', request_method='POST', permission='order:modify')
def orderlinecustomproduct_save(request):
    # Specific case: only update quantity_ordered via ajax
    if request.matchdict.get('id'):
        return orderlinecustomproduct_save_quantity_ordered(request)
    # 2 possible scenarios here: saving a new custom product or updating an existing one
    if request.params.get('id'): # Updating an existing custom product
        orderlinecustomproduct = request.dbsession.query(OrderLineCustomProduct)\
        .options(
            joinedload(OrderLineCustomProduct.order)
            .joinedload(Order.client)
        ).get(request.params.get('id'))
    else: # Saving new custom product
        clientname = request.matchdict.get('clientname')
        refdate = request.matchdict.get('refdate')
        order = dbapi.get_order_and_create_it_if_needed(clientname, util.normalize_refdate(refdate))
        orderlinecustomproduct = OrderLineCustomProduct()
        orderlinecustomproduct.order_id = order.id
        request.dbsession.add(orderlinecustomproduct)
    
    orderlinecustomproduct.price = float(request.params.get('price', orderlinecustomproduct.price))
    orderlinecustomproduct.productname = request.params.get('productname', orderlinecustomproduct.productname)
    orderlinecustomproduct.category_name = request.params.get('category_name', orderlinecustomproduct.category_name)
    orderlinecustomproduct.quantity_ordered = request.params.get('quantity_ordered', orderlinecustomproduct.quantity_ordered)
    orderlinecustomproduct.description = request.params.get('description', orderlinecustomproduct.description)
    request.dbsession.flush()
    request.dbsession.refresh(orderlinecustomproduct)
    return HTTPFound(location=request.route_path('order', clientname=orderlinecustomproduct.order.client.shortname, refdate=orderlinecustomproduct.order.refdate))


def orderlinecustomproduct_save_quantity_ordered(request):
    orderlinecustomproduct = request.dbsession.query(OrderLineCustomProduct)\
        .get(request.matchdict.get('id'))
    data = request.json_body
    if 'quantity_ordered' in data:
        newqty = data['quantity_ordered']
        if newqty >= 0:
            orderlinecustomproduct.quantity_ordered = newqty
    request.dbsession.merge(orderlinecustomproduct)
    return {
        "success": True,
        "quantity_ordered": orderlinecustomproduct.quantity_ordered,
    }


@view_config(route_name='order.clone', renderer='json', request_method='POST', permission='order:modify')
def order_clone(request):
    clientname = request.json_body.get('clientname')
    try:
        refdate_src = datetime.strptime(request.json_body.get('refdate_src'), '%Y-%m-%d')
    except:
        refdate_src = request.json_body.get('refdate_src')
    refdate_src = util.normalize_refdate(refdate_src)
    try:
        refdate_dst = datetime.strptime(request.json_body.get('refdate_dst'), '%Y-%m-%d')
    except:
        refdate_dst = request.json_body.get('refdate_dst')
    refdate_dst = util.normalize_refdate(refdate_dst)
    
    # Check that the target order can be edited
    config_manager = EclairConfigManager(request.dbsession)
    orderlockdate = config_manager.get_orderlockdatetime(refdate_dst)
    if orderlockdate < datetime.now():
        return {
            "failure": "La commande {} ne peut pas être éditée.".format(refdate_dst),
        }
    else:
        dbapi.clone_order(clientname, refdate_src, refdate_dst)
    
    return {
        "success": True,
    }
