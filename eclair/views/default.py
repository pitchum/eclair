import logging; log = logging.getLogger(__name__)
from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPForbidden

from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm import joinedload
import json

from ..models import User, Role, RolePermission, Workplace
from pyramid.security import remember, forget, authenticated_userid,NO_PERMISSION_REQUIRED


@view_config(route_name='login', renderer='../templates/login.jinja2', permission=NO_PERMISSION_REQUIRED)
def login(request):
    next_url = request.session.get('next')
    if not next_url:
        next_url = request.route_path('home')
    if authenticated_userid(request):
        return HTTPFound(location=next_url)
    message = ''
    login = ''
    if 'password' in request.params and request.method == 'POST':
        login = request.params['username']
        password = request.params['password']
        user = request.dbsession.query(User).options(joinedload(User.workplace)).filter_by(username=login).first()
        if user is not None and user.check_password(password):
            request.session['username'] = login
            if user.workplace:
                request.session['workplace'] = user.workplace.shortname
            headers = remember(request, user.username)
            return HTTPFound(location=next_url, headers=headers)
        else:
            # When db is empty, consider the first user as superadmin
            if not request.dbsession.query(User).first():
                log.warning("DB was empty. First visitor becomes superadmin")
                su = User()
                su.username = 'superadmin'
                su.set_password(password or 'superadmin')
                request.dbsession.merge(su)
                request.session['username'] = 'superadmin'
                headers = remember(request, 'superadmin')
                return HTTPFound(location=next_url, headers=headers)
        request.session.flash('Authentification échouée')
        return HTTPFound(location=request.path)

    return dict(
        message=message,
        url=request.route_url('login'),
        next_url=next_url,
        login=login,
    )


@view_config(route_name='logout', permission=NO_PERMISSION_REQUIRED)
def logout(request):
    headers = forget(request)
    next_url = request.route_path('dashboard')
    request.session.clear()
    request.session.invalidate()
    headers = forget(request)
    return HTTPFound(location=request.route_url('login'), headers=headers)


@view_config(route_name='dashboard', renderer='../templates/dashboard.jinja2')
def dashboard(request):
    user = request.dbsession.query(User).options(joinedload(User.roles)).get(request.unauthenticated_userid)
    return {
        "user": user,
    }


@view_config(route_name='home', renderer='json', permission=NO_PERMISSION_REQUIRED)
def home(request):
    userid = request.unauthenticated_userid
    if not userid:
        return HTTPFound(location=request.route_path('login'))
    user = request.dbsession.query(User)\
        .options(joinedload(User.roles))\
        .options(joinedload(User.workplace))\
        .get(userid)
    next_url = request.route_path('dashboard')
    if not user.workplace:
        return HTTPFound(location=next_url)
    if user.has_role('seller'):
        next_url = request.route_path('order', clientname=user.workplace.shortname, refdate='tomorrow')
    if user.has_role('carrier'):
        next_url = request.route_path('delivery', factoryname=user.workplace.shortname, refdate='today')
    if user.has_role('producer'):
        next_url = request.route_path('production_plan', factoryname=user.workplace.shortname, refdate='today')
    return HTTPFound(location=next_url)


@view_config(route_name='route_path', renderer='json', permission=NO_PERMISSION_REQUIRED)
def route_path(request):
    '''Special view to make pyramid's ``request.route_path()`` callable
    from javascript code using ajax.'''
    data = request.json_body
    route_name = data.get('route_name')
    params = data.get('params', {})
    return {
        'route_path': request.route_path(route_name, **params),
    }


# cf. http://codelike.com/blog/2014/04/27/a-rest-api-with-python-and-pyramid/
# TODO squash/replace useless code
class BaseView(object):
    item_cls = None  # Override this in child views
    schema_cls = None  # Override this in child views

    def __init__(self, request):
        self.request = request
        # Don't load a single item for the collection route
        if request.matched_route.name == self.item_route:
            item_id = int(request.matchdict['id'])
            item = self.request.dbsession.query(self.item_cls).get(item_id)
            self.item = item

    def create_item(self):
        data = self.request.json_body
        item = self.item_cls(**data)
        self.request.dbsession.add(item)
        self.request.dbsession.flush()
        return item

    def list_items(self):
        return self.request.dbsession.query(self.item_cls).all()

    def read_item(self):
        return self.item

    def update_item(self):
        data = self.request.json_body
        for key, value in data.items():
            setattr(self.item, key, value)
        return self.item

    def delete_item(self):
        self.request.dbsession.delete(self.item)
        return HTTPOk()


# TODO squash/replace useless code
class register_views(object):
    def __init__(self, route=None, collection_route=None):
        self.route = route
        self.collection_route = collection_route

    def __call__(self, cls):
        cls.item_route = self.route
        if self.route:
            cls = view_config(_depth=1, renderer='json', attr='read_item',
                request_method='GET', route_name=self.route, permission=self.route + ':view', xhr=True)(cls)
            cls = view_config(_depth=1, renderer='json', attr='update_item',
                request_method='PUT', route_name=self.route, permission=self.route + ':modify', xhr=True)(cls)
            cls = view_config(_depth=1, renderer='json', attr='delete_item',
                request_method='DELETE', route_name=self.route, permission=self.route + ':delete', xhr=True)(cls)
        if self.collection_route:
            cls = view_config(_depth=1, renderer='json', attr='list_items',
                request_method='GET', route_name=self.collection_route, permission=self.route + ':view', xhr=True)(cls)
            cls = view_config(_depth=1, renderer='json', attr='create_item',
                request_method='POST', route_name=self.collection_route, permission=self.route + ':modify', xhr=True)(cls)
        return cls
