import logging; log = logging.getLogger(__name__)
from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPForbidden

from sqlalchemy import distinct
from ..security import permissions
from ..models import User, Role, RolePermission, Workplace, ProductCategory

from sqlalchemy.orm import joinedload


@view_config(route_name='users', renderer='json', request_method='POST', permission='user:modify')
def user_create(request):
    data = request.json_body
    username = data.get('username')
    existing = request.dbsession.query(User).get(username)
    if existing:
        return {
            "success": False,
            "error": "Username {} already exist.".format(username),
        }
    user = User()
    user.username = username
    user.email = ''
    user.screenname = "M. ou Mme {}".format(username)
    request.dbsession.add(user)
    return {
        "success": True,
        "user": user,
    }


@view_config(route_name='users', renderer='../templates/users.jinja2', permission='user:modify')
def users_view(request):
    users = request.dbsession.query(User)\
        .options(joinedload(User.roles))\
        .order_by(User.username)\
        .all()
    return {
        "users": users,
    }


@view_config(route_name='user', renderer='../templates/user_edit.jinja2', permission='user:modify')
def user_edit(request):
    if 'username' not in request.matchdict:
        log.error('Calling this view without an username is not supported yet!')
        return HTTPFound(location=request.route_path('users'))
    user = request.dbsession.query(User).get(request.matchdict.get('username'))
    workplaces = request.dbsession.query(Workplace).order_by(Workplace.shortname).all()
    available_roles = request.dbsession.query(Role).all()
    available_product_categories = request.dbsession.query(ProductCategory).order_by(ProductCategory.name).all()
    return {
        "user": user,
        "workplaces": workplaces,
        "available_roles": available_roles,
        "user_roles": [r.rolename for r in user.roles],
        "available_product_categories": available_product_categories,
        "user_product_categorynames": [cat.name for cat in user.product_categories],
    }


@view_config(route_name='user', renderer='json', request_method='POST', permission='user:modify')
def user_update(request):
    user = request.dbsession.query(User).get(request.matchdict.get('username'))
    data = request.params
    user.username = data.get('username', user.username)
    user.screenname = data.get('screenname', user.screenname)
    if data.get('password', ''):
        user.set_password(data.get('password', user.password))
    if data.get('workplace_id', '').isnumeric():
        user.workplace_id = data.get('workplace_id', user.workplace_id)
    user.email = data.get('email', user.email)
    
    # Update user roles
    rolenames = data.getall('roles')
    _roles_to_remove = []
    for r in user.roles:
        if r.rolename not in rolenames:
            _roles_to_remove.append(r)
    for r in _roles_to_remove:
        user.roles.remove(r)
    _roles_to_add = request.dbsession.query(Role).filter(Role.rolename.in_(rolenames))
    for r in _roles_to_add:
        user.roles.append(r)
    
    # update product_categories
    user_productcategorynames = data.getall('categorynames')
    _user_productcategories_to_remove = []
    for cat in user.product_categories:
        if cat.name not in user_productcategorynames:
            _user_productcategories_to_remove.append(cat)
    for cat in _user_productcategories_to_remove:
        user.product_categories.remove(cat)
    _user_productcategories_to_add = request.dbsession.query(ProductCategory).filter(ProductCategory.name.in_(user_productcategorynames))
    for cat in _user_productcategories_to_add:
        user.product_categories.append(cat)
    
    request.dbsession.merge(user)
    return HTTPFound(location=request.route_path('users'))


@view_config(route_name='user', renderer='json', request_method='DELETE', permission='user:modify')
def user_delete(request):
    user = request.dbsession.query(User).get(request.matchdict.get('username'))
    request.dbsession.delete(user)
    return {
        "success": True,
    }

@view_config(route_name='roles', renderer='../templates/roles.jinja2', permission='user:modify')
def roles_view(request):
    roles = request.dbsession.query(Role).all()
    return {
        "roles": roles
    }


@view_config(route_name='role', renderer='../templates/role_edit.jinja2', request_method='GET', permission='user:modify')
def role_edit(request):
    role = request.dbsession.query(Role).get(request.matchdict.get('rolename'))
    current_permissions = [p.permname for p in role.permissions]
    available_permissions = [permname for permname in permissions if permname not in current_permissions]
    return {
        "role": role,
        "permissions": available_permissions,
    }


@view_config(route_name='user_role', renderer='json', request_method='PUT', permission='user:modify')
@view_config(route_name='user_role', renderer='json', request_method='POST', permission='user:modify')
def user_role_update(request):
    data = request.json_body
    username = data.get('username')
    rolename = data.get('rolename')
    user = request.dbsession.query(User).options(joinedload(User.roles)).get(username)
    already_there = False
    for r in user.roles:
        if r.rolename == rolename:
            log.warning("User {} already has role {}".format(username, rolename))
            already_there = True
            break
    if not already_there:
        role = request.dbsession.query(Role).get(rolename)
        user.roles.add(role)
        request.dbsession.merge(user)
    return {
        "success": True,
    }

@view_config(route_name='user_role', renderer='json', request_method='GET', permission='user:modify')
@view_config(route_name='user_role', renderer='json', request_method='DELETE', permission='user:modify')
def user_role_delete(request):
    data = request.json_body
    username = data.get('username')
    rolename = data.get('rolename')
    user = request.dbsession.query(User).options(joinedload(User.roles)).get(username)
    role_found = False
    for r in user.roles:
        if r.rolename == rolename:
            role_found = r
            break
    if role_found:
        user.roles.remove(r)
    return {
        "success": True,
        "message": "Role {} removed for user {}".format(rolename, username),
    }


@view_config(route_name='role_permissions', renderer='json', request_method='PUT', permission='user:modify')
@view_config(route_name='role_permissions', renderer='json', request_method='POST', permission='user:modify')
def role_permissions_update(request):
    data = request.json_body
    rp = RolePermission(**data)
    request.dbsession.add(rp)
    return {
        "success": True,
    }


@view_config(route_name='role_permissions', renderer='json', request_method='DELETE', permission='user:modify')
def role_permissions_delete(request):
    data = request.json_body
    existing = request.dbsession.query(RolePermission).get(data.values())
    request.dbsession.delete(existing)
    return {
        "success": True,
    }
