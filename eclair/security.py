import logging; log = logging.getLogger(__name__)
from collections import OrderedDict
from pyramid.authentication import AuthTktAuthenticationPolicy, SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.session import SignedCookieSessionFactory

from .models import User, Role, RolePermission
from sqlalchemy.orm import joinedload

from pyramid.security import (
    Allow,
    Authenticated,
    Everyone,
    ALL_PERMISSIONS,
    has_permission,
    unauthenticated_userid,
)


def includeme(config):
    settings = config.get_settings()
    
    my_session_factory = SignedCookieSessionFactory(
        settings.get('session.cookie_signing_secret', 'itsaseekreet'),
        timeout=settings.get('session.timeout', 3600)
    )
    config.set_session_factory(my_session_factory)
    
    authn_policy = SessionAuthenticationPolicy(callback=get_principals)
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(ACLAuthorizationPolicy())
    config.set_default_permission('authenticated')
    config.add_request_method(get_userdata, 'userdata', reify=True)
    config.add_request_method(get_roles, 'roles', reify=True)
    config.set_root_factory(RootFactory)


def generate_acl(request):
    '''
    Build from database the list of ``__acl__`` formatted
    appropriately to accomodate ``pyramid.security``.
    '''
    result = [
        (Allow, Authenticated, 'authenticated'),
        (Allow, Authenticated, 'product:read'),
        (Allow, 'admin', ALL_PERMISSIONS),
    ]
    for row in request.dbsession.query(RolePermission):
        result.append((Allow, row.rolename, row.permname))
    return result

def get_userdata(request):
    '''Using HTTP session as cache for storing user's information.'''
    if 'userdata' not in request.session:
        user_id = request.unauthenticated_userid
        if user_id is not None:
            log.warn("DB query with user_id = '{}'".format(user_id))
            user = request.dbsession.query(User)\
                .options(joinedload(User.product_categories))\
                .options(joinedload(User.roles))\
                .options(joinedload(User.workplace))\
                .filter(User.username == user_id)\
                .first()
            request.session['userdata'] = {
                'username': user.username,
                'workplace_shortname': user.workplace.shortname if user.workplace else '',
                'rolenames': [r.rolename for r in user.roles],
                'product_categorynames': [cat.name for cat in user.product_categories],
            }
    return request.session.get('userdata')


def get_roles(request):
    """
    Fetch from database the rolenames associated with the currently
    authenticated user.
    
    This method is not supposed to be called directly. It is available
    as a request reified attribute: ``request.roles``.
    
    :return:
        A list of rolenames (simple strings).
    """
    log.warning("Deprecated call to request.roles. Use request.userdata.get('rolenames') instead")
    return request.userdata.get('rolenames')


def get_principals(userid, request):
    """
    Return all *principals* of an authenticated user.
    
    *Principal* is a standard term in authentification lexical field
    that can be read as "role".
    
    :param userid:
        username already authenticated with ``pyramid.security.remember``.
    :param request:
        Current HTTP request.
    :return:
        A list of *principals* (simple strings).
    """
    return request.userdata.get('rolenames')


class RootFactory(object):
    """
    TODO quickly explain how RootFactory is involved in authn/authz mechanisms.
    
    Tip: command line to list all permissions currently expected in view callables and templates.
    
    grep --include=*.py -r 'permission \?=' eclair/views/ | sed "s/.*permission \?= \?u\?'\([^']\+\)'.*/\1/" | sort | uniq -c | sort -n
    grep --include=*jinja2 "has_permission" -r eclair/templates | sed "s/.*has_permission(u\?'\([^']\+\)').*/\1/" | sort | uniq -c
    """

    def __init__(self, request):
        if not request.path.startswith('/static/'):
            self.__acl__ = generate_acl(request)
        else:
            self.__acl__ = []


permissions = OrderedDict([
    ('config:view', ''' '''),
    ('config:modify', ''' '''),

    ('order:view'  , '''Permission to view any order.'''),
    ('order:modify', '''Permission to create or modify an order.'''),
    ('order:delete', '''Permission to delete an order.'''),
    
    ('product:view'  , '''Permission to view an existing product.'''),
    ('product:modify', '''Permission to create or modify a product.'''),
    ('product:delete', '''Permission to delete an existing product.'''),
    
    ('workplace:view'  , ''' '''),
    ('workplace:modify', ''' '''),
    ('workplace:delete', ''' '''),
    
    ('user:view'  , ''' '''),
    ('user:modify', ''' '''),
    ('user:delete', ''' '''),
])
