import unittest
import transaction

from pyramid import testing


def dummy_request(dbsession):
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('.models')
        settings = self.config.get_settings()

        from .models import (
            get_engine,
            get_session_factory,
            get_tm_session,
            )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        from .models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        from .models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)


class TestMyViewSuccessCondition(BaseTest):

    def setUp(self):
        super(TestMyViewSuccessCondition, self).setUp()
        self.init_database()

        from .models import User

        model = User(username='superadmin', description="Special account with all permissions")
        self.session.add(model)

    def test_passing_view(self):
        from .views.default import debug
        info = debug(dummy_request(self.session))
        self.assertEqual(info['test'], 'test')


class TestMyViewFailureCondition(BaseTest):

    def test_failing_view(self):
        from .views.default import debug
        info = debug(dummy_request(self.session))
        self.assertEqual(info.status_int, 500)


class TestProductViews(BaseTest):

    def setUp(self):
        super(TestProductViews, self).setUp()
        self.init_database()
        from .models import Product

        model = Product(shortname='croissant', name='croissant au beurre', description="Viennoiserie à base de beurre")
        self.session.add(model)

    def test_list_products(self):
        from .views.product import ProductView
        req = dummy_request(self.session)
        mr = type("Route", (object,), {})()
        mr.name = 'products'
        req.matched_route = mr
        req.matchdict = {"id": 42}
        r = ProductView(req).list_items()
        self.assertEqual(r['product']['shortname'], 'fakeproduct')

    def test_get_product(self):
        from .views.product import ProductView
        req = dummy_request(self.session)
        mr = type("Route", (object,), {})()
        mr.name = 'products'
        req.matched_route = mr
        r = ProductView(req).list_items()
        self.assertEqual(r['product']['shortname'], 'fakeproduct')
