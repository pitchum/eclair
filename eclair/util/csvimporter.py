import logging
log = logging.getLogger(__name__)
import csv, re, unidecode, random
from datetime import datetime

from ..models import Workplace, Product, ProductCategory, ProductSubcategory


class CsvCatalogueImporter(object):
    
    def __init__(self, dbsession, fieldnames=('category', 'subcategory', 'product_name'), **kwargs):
        self.fieldnames = fieldnames
        self.dbsession = dbsession
        self.start_time = datetime.now()
        self.ref_increment = dict()
        self.dry_run = kwargs.get('dry_run', False)
    
    def _iter_products(self, filename):
        with open(filename, encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',', fieldnames=self.fieldnames)
            for row in reader:
                if row.get('product_name'):
                    yield row
    
    
    def import_products_from_file(self, filename):
        cats = set()
        subcats = set()
        prods = set()
        
        factory = self.dbsession.query(Workplace).filter(Workplace.factory).first()
        if not factory:
            log.warning("No factory present in DB. Products will be inserted unassigned.")
        
        for product in self._iter_products(filename):
            prodname = product.get('product_name')
            if prodname is None:
                continue
            cat = product.get('category')
            subcat = product.get('subcategory')
            if cat.upper() not in cats:
                cats.add(cat.upper())
                pc = ProductCategory()
                pc.name = cat
                pc.description = ''
                if not self.dry_run:
                    self.dbsession.merge(pc)
                    self.dbsession.commit()
                #  print('''sqlite3 dbdev.sqlite "insert into product_category (name) values ('{}')"'''.format(cat.replace("'", "''")))
            if subcat.upper() not in subcats:
                subcats.add(subcat.upper())
                psc = ProductSubcategory()
                psc.name = subcat
                psc.category_name = cat
                psc.description = ''
                if not self.dry_run:
                    self.dbsession.merge(psc)
                    self.dbsession.commit()
                #  print('''sqlite3 dbdev.sqlite "insert into product_subcategory (name, category_name) values ('{}', '{}')"'''.format(subcat.replace("'", "''"), cat.replace("'", "''")))
            if prodname.startswith('...'):
                continue
            if prodname not in prods:
                prods.add(prodname)
                p = Product()
                self._scan_prodname(p, prodname)
                p.subcategory_name = subcat
                p.creationdate = self.start_time
                p.lastupdate = self.start_time
                p.price = random.randint(10, 250) / 10
                if factory:
                    p.production_center_id = factory.id
                if not self.dry_run:
                    self.dbsession.add(p)
                #  print('''sqlite3 dbdev.sqlite "insert into product (subcategory_name, shortname) values ('{}', '{}')"'''.format(subcat, prodname))
            else:
                print("echo \"-- ignoring duplicate: '{}'\"".format(prodname))

    def _scan_prodname(self, product, prodname):
        orig = prodname
        p = product
        p.packaging = ''
        if '(pièce)' in prodname:
            p.packaging = 'pièce'
            prodname = prodname.replace('(pièce)', '').strip()
        elif '(plaque)' in prodname:
            p.packaging = 'plaque'
            prodname = prodname.replace('(plaque)', '').strip()
        
        if True:
            for regex in ('\((x[0-9]+)\)', '([0-9/]+P?)', ):
                m = re.search(regex, prodname)
                if m:
                    p.packaging = m.group(1)
                    prodname = re.sub(regex, '', prodname)
                    log.debug('''product name match regex: '%s' => '%s' (regex="%s")''', orig, prodname, regex)
        
        p.shortname = orig
        p.longname = orig
        p.reference = self._generate_reference(prodname)
        

    def _generate_reference(self, prodname):
        res = unidecode.unidecode(prodname)
        res = res.title()
        res = re.sub('''[a-z ()'"-]''', '', res)
        res = res.replace('...', '')
        res = res.strip()
        if len(res) < 2:
            log.debug("Reference too short: '%s'", res)
            res = re.sub('''[ ()\./'"-]''', '', unidecode.unidecode(prodname).upper())
            res = res[0:min(3,len(res))]
        if res not in self.ref_increment:
            self.ref_increment[res] = 0
            return res
        self.ref_increment[res] = 1 + self.ref_increment[res]
        res = res + str(self.ref_increment[res])
        return res
