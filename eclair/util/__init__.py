import logging; log = logging.getLogger(__name__)
from datetime import datetime, timedelta, time
import calendar, re, unidecode
from eclair.models import allmodels as mdl
from eclair import util


def normalize_refdate(refdate):
    '''Convert 'today', 'yesterday' or 'tomorrow' into a compact date string.
    If refdate is already formatted, return it untouched.
    Eg. '20171028' '''
    if refdate in ('today', 'tomorrow'):
        _now = datetime.now()
        if refdate == 'today':
            refdate = _now.strftime('%Y%m%d')
        elif refdate == 'tomorrow':
            tomorrow = _now + timedelta(days=1)
            refdate = tomorrow.strftime('%Y%m%d')
        elif refdate == 'yesterday':
            tomorrow = _now + timedelta(days=-1)
            refdate = tomorrow.strftime('%Y%m%d')
    if isinstance(refdate, datetime):
        refdate = refdate.strftime('%Y%m%d')
    return refdate


def parse_refdate(refdate):
    '''Convert 'today', 'yesterday', 'tomorrow' or '20170120' into a datetime'''
    return datetime.strptime(normalize_refdate(refdate), '%Y%m%d')


def refdates(refdate):
    refdate = normalize_refdate(refdate)
    prev_refdate = (parse_refdate(refdate) + timedelta(days=-1)).strftime('%Y%m%d')
    next_refdate = (parse_refdate(refdate) + timedelta(days=1)).strftime('%Y%m%d')
    return prev_refdate, refdate, next_refdate


def refdates_range(start_refdate, daycount=7):
    start_refdate = normalize_refdate(start_refdate)
    res = [start_refdate]
    for i in range(1, daycount):
        res.append((datetime.strptime(start_refdate, '%Y%m%d') + timedelta(days=i)).strftime('%Y%m%d'))
    return res


def normalize_refmonth(refmonth):
    '''Convert 'current', 'last' and 'next' into a compact month string.
    If refmonth is already formatted, return it untouched.
    Eg. '201801' '''
    if refmonth in ('last', 'current', 'next'):
        _now = datetime.now()
        if refmonth == 'current':
            refmonth = _now.strftime('%Y%m')
        elif refmonth == 'last':
            tomorrow = _now + timedelta(months=-1)
            refmonth = tomorrow.strftime('%Y%m')
        elif refmonth == 'next':
            tomorrow = _now + timedelta(months=1)
            refmonth = tomorrow.strftime('%Y%m')
    return refmonth


def parse_refmonth(refmonth):
    '''Convert 'current', 'last', 'next' or '201801' into a datetime representing a month.'''
    return datetime.strptime(normalize_refmonth(refmonth), '%Y%m')


def refmonth_daterange(refmonth):
    dt_start = parse_refmonth(refmonth)
    last_day = calendar.monthrange(dt_start.year, dt_start.month)[1]
    dt_end = dt_start.replace(day=last_day)
    return dt_start, dt_end
    

def refmonth_range(refmonth, before=1, after=1):
    dt_refmonth = parse_refmonth(refmonth)
    dt_start_month = (dt_refmonth + timedelta(days=-(30.5 * before))).replace(day=1)
    dt_end_month = (dt_refmonth + timedelta(days=(30.5 * after))).replace(day=1) # XXX unpredictible results
    res = []
    cur = dt_start_month
    while cur < dt_end_month:
        res.append((cur).strftime('%Y%m'))
        cur = (cur + timedelta(days=31)).replace(day=1)
    return res


def _generate_reference(production_center_name, subcatname, prodname, maxlen=18):
    longname = unidecode.unidecode(subcatname + " " + prodname)
    res = longname.title()
    res = re.sub('''[a-z ()'"-]''', '', res)
    res = res.replace('...', '')
    res = res.replace('.', '')
    res = res.replace('…', '')
    res = res.replace('/', '-')
    res = res.replace('&', '')
    res = res.strip()
    res = production_center_name + res
    if not res.isalnum():
        log.warning("Reference contains non alphanums characters: '%s'. Cleaning", res)
        cleaned_res = ''
        for c in res:
            if c.isalnum():
                cleaned_res += c
        res = cleaned_res
    if len(res) >= maxlen:
        badref = res
        res = res[0:maxlen]
        log.warning("Reference is longer than %d characters. Truncating. '%s' -> '%s'", maxlen, badref, res)
    return res


class EclairConfigManager(object):
    ''' Read/Write access to application settings stored in database'''
    
    def __init__(self, dbsession):
        self.dbsession = dbsession
        self.cache = None
    
    def update_orderlocktime(self, orderlocktime):
        try:
            time_ = datetime.strptime(orderlocktime, '%H:%M').time()
        except:
            raise Exception('''Format de donnée non valide l'heure de verrouillage de commande: "{}"'''.format(orderlocktime))
        return self.set('orderlocktime', orderlocktime)
    
    def _load_config_if_needed(self):
        if self.cache != None:
            return
        self.cache = {cfgitem.key:cfgitem.val for cfgitem in self.dbsession.query(mdl.AppSettings).all()}
    
    def get(self, *args, **kwargs):
        if len(args) == 1:
            return self.asdict().get(args[0])
        elif len(args) == 2:
            return self.asdict().get(args[0], args[1])
    
    def get_float(self, *args, **kwargs):
        raw_val = self.get(*args, **kwargs)
        return float(raw_val)
    
    def set(self, key, val):
        self._load_config_if_needed()
        current_row = self.dbsession.query(mdl.AppSettings).filter(mdl.AppSettings.key==key).first()
        if not current_row:
            current_row = mdl.AppSettings(key=key, val=val)
            self.dbsession.merge(current_row)
        else:
            current_row.val = val
            self.dbsession.merge(current_row)
        self.cache[key] = val
        return current_row.val
    
    def asdict(self):
        self._load_config_if_needed()
        return self.cache

    
    def get_orderlockdatetime(self, refdate):
        '''Returns a datetime corresponding to the last moment an order
        may be modified, depending on its refdate.'''
        orderlocktime = datetime.strptime(self.get('orderlocktime', '00:00'), '%H:%M').timetuple()
        orderlockdate = util.parse_refdate(refdate).replace(hour=orderlocktime.tm_hour, minute=orderlocktime.tm_min, second=0, microsecond=0) + timedelta(days=-1)
        return orderlockdate
