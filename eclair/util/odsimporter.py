import logging
log = logging.getLogger(__name__)
import ezodf, re, unidecode, random, os, json
from datetime import datetime
from collections import OrderedDict

from ..models import allmodels as mdl
import sqlalchemy as sa


# Category and subcategory names must match this pattern
regex_valid_name = re.compile('^[A-Za-z][A-Za-z0-9_-]+$')

class OdsImporter(object):

    def __init__(self, dbsession, **kwargs):
        self.dry_run = kwargs.get('dry_run', False)
        self.db = DbWrapper(dbsession)
        self.res = AnalysisResult()
        self.cache_filename = '/tmp/eclair_analyze.json'
    
    def analyze(self, filenames):
        for filename in filenames:
            log.debug("Analyzing {}".format(os.path.basename(filename)))
            doc = ezodf.opendoc(filename)
            curr_prodcenter_name = None
            for candidate in doc.sheets:
                if 'Fact' in candidate.name:
                    factory_section, curr_prodcenter_name = self._analyze_invoice_sheet(candidate)
                    # self.res._register_factory_section_for_prodcenter(curr_prodcenter_name, factory_section)
            if curr_prodcenter_name == None:
                log.warning("Could not guess prodcenter name. Ignoring file %s", os.path.basename(filename))
                continue
            for candidate in doc.sheets:
                if 'Prod' in candidate.name:
                    self._analyze_prod_sheet(candidate, curr_prodcenter_name)
            for candidate in doc.sheets:
                if 'Livr_jour' in candidate.name:
                    pass
                elif 'Prod' in candidate.name:
                    pass # already done, see above
                elif 'Fact' in candidate.name:
                    pass # already done, see above
                elif len(candidate.name) == 3:
                    self.res._register_client_for_prodcenter(curr_prodcenter_name, factory_section, candidate.name)
                    self._analyze_customer_sheet(candidate, curr_prodcenter_name)
                else:
                    log.warning("Unsupported sheetname. Ignoring sheet '%s' (in %s)", candidate.name, os.path.basename(filename))
            ezodf.config.reset_table_expand_strategy()
        
        if self.cache_filename:
            with open(self.cache_filename, 'w') as f:
                json.dump(self.res._as_dict(), f, indent='    ')
        
        return self.res._as_dict()
    
    def _analyze_invoice_sheet(self, sheet):
        '''Extract the following information:
        - factory name
        - product categories
        - workplace shortname
        - workplaces legal info (legal name, address, ...)
        - product unit prices
        '''
        invoice_number = sheet['F5'].plaintext().split(':')[1].strip()
        vi, factoryname, year, month, wpname, clientname = invoice_number.split('-')
        # legal info of factory
        # TODO
        
        # legal info of clients
        if sheet.ncols() > 8:
            row_index = 0
            for i, row in enumerate(sheet.rows()):
                if row[4].plaintext() == 'Total HT':
                    row_index = i
                    break
            for i in range(8, sheet.ncols() - 1):
                client_shortname  = sheet.get_cell((11, i)).plaintext()
                client_legal_name = sheet.get_cell((row_index, i)).plaintext()
                client_address    = sheet.get_cell((row_index + 1, i)).plaintext()
                client_legal_info = sheet.get_cell((row_index + 2, i)).plaintext()
                
                self.res._register_workplace_legal_info(client_shortname, client_legal_name, client_address, client_legal_info)
        
        # product prices
        start_of_products = None
        for i, row in enumerate(sheet.rows()):
            if not start_of_products:
                if row[3].plaintext() == 'Volume':
                    start_of_products = True
                continue
            tmp = [c.plaintext() for c in row[0:5]]
            # log.info(tmp)
            cat, subcat, prodname, tmp, price = tmp
            self.res._register_product(wpname, cat, subcat, prodname, price=price)
        
        return factoryname, wpname
    
    def _analyze_prod_sheet(self, sheet, production_center_name):
        for row in self._iter_prod_sheet(sheet):
            cat, subcat, product_name = row
            self.res._register_product(production_center_name, cat, subcat, product_name)
    
    def _analyze_customer_sheet(self, sheet, production_center_name):
        for row in self._iter_prod_sheet(sheet):
            cat, subcat, product_name = row
            self.res._register_product(production_center_name, cat, subcat, product_name, clientname=sheet.name)
    
    def import_files(self, filenames):
        if self.cache_filename and os.path.exists(self.cache_filename):
            with open(self.cache_filename, 'r') as f:
                data = json.load(f)
        else:
            data = self.analyze(filenames)
        # Update workplaces info
        for wpname, info in data.get('workplaces', {}).items():
            self.db.upsert_workplace(wpname, factory_sections=info.get('prod_sections'), shop=True, legal_info=info.get('legal_info', ''))
        # Update catalogues
        for ref, product in data.get('products', {}).items():
            cat = clean_name(product.get('cat')) + '_' + product.get('production_center_name')
            subcat = clean_name(product.get('subcat')) + '_' + product.get('production_center_name')
            self.db.upsert_product_category(cat)
            self.db.upsert_product_subcategory(cat, subcat)
            product_id = self.db.upsert_product(ref, subcat, product.get('name'), product.get('production_center_name'), product.get('price'))
            # Update links between product and customers
            self.db.update_product_client_links(product_id, product.get('clients'))
    
    def _iter_prod_sheet(self, sheet, col_range=(0, 3)):
        for row_index, row in enumerate(sheet.rows()):
            if row_index < 2: # XXX hard-coded value. TODO autodetect first interesting line
                continue
            row = sheet.row(row_index)
            # Exclude some special rows
            if row[1].style_name == 'ce4': # XXX hard-coded value obtained experimentally
                log.debug("Ignoring 'black' line %2d %s", row_index, [row[i].plaintext() for i in range(*col_range)])
                continue
            res = [row[i].plaintext().strip() for i in range(*col_range)]
            if not res[0] or not res[1] or not res[2]:
                continue
            yield res


class DbWrapper(object):
    
    def __init__(self, dbsession):
        self.dbsession = dbsession
        self.cache = {}
        self.start_time = datetime.now()
    
    def upsert_product_category(self, catname):
        self._load_existing_product_category_names()
        if catname not in self.cache['product_category_names']:
            log.debug("Inserting product_category %s", catname)
            cat = mdl.ProductCategory()
        else:
            cat = self.dbsession.query(mdl.ProductCategory)\
                .filter(mdl.ProductCategory.name == catname)\
                .first()
        cat.name = catname
        #cat.production_center_id = production_center_id
        cat.vatrate = 5.5
        self.dbsession.add(cat)
        self.dbsession.commit()
        self.cache['product_category_names'].append(catname)
    
    def _load_existing_product_category_names(self):
        if 'product_category_names' not in self.cache:
            self.cache['product_category_names'] = [r.name for r in self.dbsession.query(mdl.ProductCategory.name)]
    
    def upsert_product_subcategory(self, catname, subcatname):
        self._load_existing_product_subcategory_names()
        if subcatname not in self.cache['product_subcategory_names']:
            log.debug("Inserting product_subcategory %s", subcatname)
            subcat = mdl.ProductSubcategory()
            subcat.category_name = catname
            subcat.name = subcatname
            self.dbsession.add(subcat)
            self.dbsession.commit()
            self.cache['product_subcategory_names'].append(subcatname)
    
    def _load_existing_product_subcategory_names(self):
        if 'product_subcategory_names' not in self.cache:
            self.cache['product_subcategory_names'] = [r.name for r in self.dbsession.query(mdl.ProductSubcategory.name)]
    
    def upsert_product(self, ref, subcatname, product_name, production_center_name, price):
        self._load_existing_product_names()
        if ref not in self.cache['product_refs']:
            log.debug("Inserting product %s", product_name)
            product = mdl.Product()
            self.dbsession.add(product)
        else:
            # log.debug("Updating product %s", product_name)
            pid = self.cache['product_refs'][ref]
            product = self.dbsession.query(mdl.Product).get(pid)
            self.dbsession.merge(product)
        product.production_center_id = self.get_production_center_id(production_center_name)
        product.subcategory_name = subcatname
        product.shortname = product_name
        product.longname = product_name
        product.reference = ref
        product.price = price
        product.description = ''
        product.enabled = True
        product.creationdate = self.start_time
        product.lastupdate = self.start_time
        self.dbsession.flush()
        self.dbsession.refresh(product)
        self.dbsession.commit()
        self.cache['product_refs'][product_name] = product.id
        return self.cache['product_refs'][product_name]
    
    def _load_existing_product_names(self):
        if 'product_refs' not in self.cache:
            self.cache['product_refs'] = {r.reference:r.id for r in self.dbsession.query(mdl.Product)}
    
    def upsert_workplace(self, shortname, factory_sections=None, shop=True, legal_info=None):
        self._load_existing_workplace_names()
        if shortname not in self.cache['workplace_names']:
            log.info("Inserting workplace %s", shortname)
            wp = mdl.Workplace()
            self.dbsession.add(wp)
        else:
            wp = self.dbsession.query(mdl.Workplace).filter(mdl.Workplace.shortname==shortname).first()
        wp.factory = factory_sections is not None
        wp.shop = shop
        wp.shortname = shortname
        if legal_info:
            wp.name = legal_info.get('longname')
            wp.address = legal_info.get('address')
            wp.description = legal_info.get('legal')
        self.dbsession.flush()
        # Retrieve generated id
        self.dbsession.refresh(wp)
        self.cache['workplace_names'][shortname] = wp.id
        if factory_sections:
            for section in factory_sections:
                log.info("Do upsert link between %s and %s", shortname, section)
    
    def upsert_production_center(self, production_center_name, factory_sections=[]):
        self.upsert_workplace(production_center_name)
    
    def _load_existing_workplace_names(self):
        if 'workplace_names' not in self.cache:
            self.cache['workplace_names'] = {r.shortname: r.id for r in self.dbsession.query(mdl.Workplace.shortname, mdl.Workplace.id)}
    
    def get_production_center_id(self, name):
        self._load_existing_workplace_names()
        if name not in self.cache['workplace_names']:
            self.upsert_production_center(name)
        return self.cache['workplace_names'][name]

    def update_product_client_links(self, product_id, clientnames):
        # TODO first remove existing clients linked to product
        del_ = mdl.lnk_product_client.delete().where(mdl.lnk_product_client.c.product_id==product_id)
        self.dbsession.execute(del_)
        if not clientnames:
            log.warning("Product has no clients: {}.".format(product_id))
            return
        q = self.dbsession.query(mdl.Workplace).filter(mdl.Workplace.shortname.in_(clientnames))
        for wp in q.all():
            ins = mdl.lnk_product_client.insert().values(product_id=product_id, client_id=wp.id)
            self.dbsession.execute(ins)
        self.dbsession.flush()
        self.dbsession.commit()


class AnalysisResult(object):
    
    def __init__(self):
        self.refs = {}
        self.workplaces = {}
    
    def _register_workplace_legal_info(self, shortname, *args):
        data = {"longname": args[0], "address": args[1], "legal": args[2]}
        if shortname not in self.workplaces:
            self.workplaces[shortname] = {}
        if 'legal_info' in self.workplaces[shortname]:
            before = json.dumps(self.workplaces[shortname]['legal_info'])
            after = json.dumps(data)
            if before != after:
                log.warning("Multiple different legal info found for %s:\n    before: '%s'\n    after:  '%s'", shortname, before, after)
        self.workplaces[shortname]['legal_info'] = data
    
    def _register_client_for_prodcenter(self, prodcenter_shortname, factory_section, clientname):
        if not factory_section or not len(factory_section.strip()):
            raise Exception('Bad name: "%s"', factory_section)
        if prodcenter_shortname not in self.workplaces:
            self.workplaces[prodcenter_shortname] = OrderedDict()
        if 'prod_sections' not in self.workplaces[prodcenter_shortname]:
            self.workplaces[prodcenter_shortname]['prod_sections'] = OrderedDict()
        if factory_section not in self.workplaces[prodcenter_shortname]['prod_sections']:
            self.workplaces[prodcenter_shortname]['prod_sections'][factory_section] = OrderedDict()
        if 'clients' not in self.workplaces[prodcenter_shortname]['prod_sections'][factory_section]:
            self.workplaces[prodcenter_shortname]['prod_sections'][factory_section]['clients'] = OrderedDict()
        if clientname not in self.workplaces[prodcenter_shortname]['prod_sections'][factory_section]['clients']:
            self.workplaces[prodcenter_shortname]['prod_sections'][factory_section]['clients'][clientname] = []
    
    def _register_factory_section_for_prodcenter(self, prodcenter_shortname, factory_section_name):
        if prodcenter_shortname not in self.workplaces:
            self.workplaces[prodcenter_shortname] = {}
        if 'factory_section_name' not in self.workplaces[prodcenter_shortname]:
            self.workplaces[prodcenter_shortname]['factory_section_name'] = []
        self.workplaces[prodcenter_shortname]['factory_section_name'].append(factory_section_name)
    
    def _register_product(self, prodcenter_shortname, cat, subcat, product_name, clientname=None, price=None):
        if '…' in product_name or '...' in product_name:
            log.debug("Ignoring product '%s'", product_name)
            return
        if not cat or not len(cat.strip()):
            log.debug("Ignoring product '%s' because bad category name '%s'", product_name, cat)
            return
        if prodcenter_shortname not in self.workplaces:
            self.workplaces[prodcenter_shortname] = OrderedDict()
        if 'prod_sections' not in self.workplaces[prodcenter_shortname]:
            self.workplaces[prodcenter_shortname]['prod_sections'] = OrderedDict()
        if cat not in self.workplaces[prodcenter_shortname]['prod_sections']:
            self.workplaces[prodcenter_shortname]['prod_sections'][cat] = OrderedDict()
        ref = _generate_reference(prodcenter_shortname, subcat, product_name)
        if ref not in self.refs:
            self.refs[ref] = { "cat": cat, "subcat": subcat, "name": product_name, "production_center_name": prodcenter_shortname, "price": price }
        else:
            prev = self.refs[ref]
            curr = { "subcat": subcat, "name": product_name, "production_center_name": prodcenter_shortname, "price": price }
            if prev.get('production_center_name') != curr.get('production_center_name'):
                log.warning("Reference collision. %s in both: '%s' and '%s'", ref, prev.get('production_center_name'), prodcenter_shortname)
        if clientname:
#            if 'clients' not in self.workplaces[prodcenter_shortname]['prod_sections'][cat]:
#                self.workplaces[prodcenter_shortname]['prod_sections'][cat]['clients'] = OrderedDict()
#            if clientname not in self.workplaces[prodcenter_shortname]['prod_sections'][cat]['clients']:
#                self.workplaces[prodcenter_shortname]['prod_sections'][cat]['clients'][clientname] = []
#            if ref not in self.workplaces[prodcenter_shortname]['prod_sections'][cat]['clients'][clientname]:
#                self.workplaces[prodcenter_shortname]['prod_sections'][cat]['clients'][clientname].append(ref)
            if 'clients' not in self.refs[ref]:
                self.refs[ref]['clients'] = []
            if clientname not in self.refs[ref]['clients']:
                self.refs[ref]['clients'].append(clientname)
    
    def _as_dict(self):
        return {
            "workplaces": self.workplaces,
            "products": self.refs,
        }


ref_increment = {}
def _generate_reference(production_center_name, subcatname, prodname):
    longname = unidecode.unidecode(subcatname + " " + prodname)
    res = longname.title()
    res = re.sub('''[a-z ()'"-]''', '', res)
    res = res.replace('...', '')
    res = res.replace('.', '')
    res = res.replace('…', '')
    res = res.replace('/', '')
    res = res.replace('&', '')
    res = res.strip()
    res = production_center_name + res
    if len(res) < 2:
        log.debug("Reference too short: '%s'", res)
        res = re.sub('''[ ()\./'"-]''', '', unidecode.unidecode(prodname).upper())
        res = res[0:min(3,len(res))]
    if len(res) > 10:
        prev = res
        res = res[0:10]
        # log.warning("Reference too long: '%s' -> '%s' (from '%s')", prev, res, longname)
    if not res.isalnum():
        log.warning("Reference contains non alphanums characters: '%s'", res)
    return res


def clean_name(name):
    '''Make sure the name will be URL-friendly'''
    if regex_valid_name.match(name):
        return name
    if '/' not in name:
        log.warning("Name is URL-compatible but still contains non ascii characters: '{}'".format(name))
        cleaned_name = name
    else:
        cleaned_name = unidecode.unidecode(name).replace('/', '-')
        log.warning("Replaced / with - in '{}' -> '{}'".format(name, cleaned_name))
    return cleaned_name
