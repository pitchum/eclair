import logging; log = logging.getLogger(__name__)
from datetime import datetime, timedelta
from collections import OrderedDict
from pyramid.threadlocal import get_current_request

from sqlalchemy.orm import joinedload, aliased
import sqlalchemy as sa

from ..models import Order, OrderLine, Product, Workplace, ProductSubcategory, ProductCategory, OrderLineCustomProduct
from eclair import util
from eclair.util import EclairConfigManager


def get_order_and_create_it_if_needed(clientname, refdate, dbsession=None):
    if not dbsession:
        dbsession = get_current_request().dbsession
    
    order = dbsession.query(Order)\
        .join(Order.client)\
        .filter(Workplace.shortname == clientname)\
        .filter(Order.refdate == refdate)\
        .first()
    
    if order is None: # create a row in table Order if missing
        client = dbsession.query(Workplace)\
            .filter(Workplace.shortname == clientname).first()
        if not client:
            raise Exception("Client not found: %s", clientname)
        order = Order()
        order.client_id = client.id
        order.refdate = refdate
        order.due_date = datetime.strptime(refdate, '%Y%m%d')
        dbsession.add(order)
        dbsession.flush()
        dbsession.refresh(order)
    return order


def fetch_products_indexed_by_ref(factorynames=[], clientnames=[], daterange=(), dbsession=None):
    if not dbsession:
        dbsession = get_current_request().dbsession
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    q = dbsession.query(Product)\
        .options(joinedload(Product.subcategory).joinedload(ProductSubcategory.category))\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .order_by(ProductSubcategory.category_name, Product.subcategory_name, Product.shortname)
    # apply filters
    if clientnames:
        q = q.join(client_alias, Product.clients)\
            .filter(client_alias.shortname.in_(clientnames))
    
    return OrderedDict(((p.reference,p) for p in q))


def fetch_order_data(clientname, refdate, dbsession=None):
    if not dbsession:
        dbsession = get_current_request().dbsession
    
    order = get_order_and_create_it_if_needed(clientname, refdate, dbsession)
    lines = {l.product_id:l for l in order.lines}
    
    items = OrderedDict()
    total_price = 0
    client = dbsession.query(Workplace).filter(Workplace.shortname == clientname).first()
    today = datetime.strptime(refdate, '%Y%m%d')
    bigbang = datetime.strptime('20100101', '%Y%m%d')
    future_ = datetime.strptime('22420101', '%Y%m%d')
    q =  dbsession.query(Product)\
        .options(
            joinedload(Product.subcategory)
            .joinedload(ProductSubcategory.category)
            .joinedload(ProductCategory.production_center)
        )\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .filter(ProductCategory.production_center_id != None)\
        .filter(Product.enabled == True)\
        .filter(Product.clients.contains(client))\
        .filter(today >= sa.case([(Product.validity_startdate != None, Product.validity_startdate)], else_=bigbang))\
        .filter(today <= sa.case([(Product.validity_enddate != None, Product.validity_enddate)], else_=future_))\
        .order_by(ProductSubcategory.category_name, Product.subcategory_name, Product.shortname)
    # log.debug(q)
    for p in q.all():
        item = dict(p.__json__())
        item['subcategory'] = p.subcategory
        item['factoryname'] = p.subcategory.category.production_center.shortname
        if p.id in lines and lines.get(p.id).quantity_ordered:
            item['quantity_ordered'] = float(lines.get(p.id).quantity_ordered)
            total_price += float(lines.get(p.id).quantity_ordered) * (p.price or 0)
        else:
            item['quantity_ordered'] = 0
        items[p.id] = item
    return order, items, total_price


def fetch_prod_todolist(factoryname, refdate='tomorrow', product_categorynames=None, dbsession=None):
    if not dbsession:
        dbsession = get_current_request().dbsession
    refdate = util.normalize_refdate(refdate)
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    # list of clients that have ordered something from the current factory
    q1 = dbsession.query(client_alias.shortname)\
        .select_from(OrderLine)\
        .join(OrderLine.order)\
        .join(client_alias, Order.client)\
        .join(OrderLine.product)\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .filter(Order.refdate == refdate)\
        .filter(factory_alias.shortname == factoryname)\
        .filter(OrderLine.quantity_ordered > 0)\
        .group_by(client_alias.shortname)
    if product_categorynames:
        q1 = q1.filter(ProductSubcategory.category_name.in_(product_categorynames))
    clients = sorted([wp[0] for wp in q1])
    
    # a the factory one of the clients?
    factory_is_client = factoryname in clients
    
    # quantity of each product ordered by each client
    subq = OrderedDict()
    for cl in clients:
        q2 = dbsession.query(Product.reference, sa.func.sum(OrderLine.quantity_ordered))\
            .select_from(OrderLine)\
            .join(OrderLine.product)\
            .join(Product.subcategory)\
            .join(ProductSubcategory.category)\
            .join(factory_alias, ProductCategory.production_center)\
            .join(OrderLine.order)\
            .join(client_alias, Order.client)\
            .filter(Order.refdate == refdate)\
            .filter(client_alias.shortname == cl)\
            .filter(OrderLine.quantity_ordered > 0)\
            .filter(factory_alias.shortname == factoryname)\
            .group_by(Product.reference)
        if product_categorynames:
            q2 = q2.filter(ProductSubcategory.category_name.in_(product_categorynames))
        subq[cl] = {ref: qty for ref, qty in q2}
    # log.debug(subq)
    # products available in the current factory
    q3 = dbsession.query(Product)\
        .options(joinedload(Product.subcategory).joinedload(ProductSubcategory.category))\
        .select_from(OrderLine)\
        .join(OrderLine.product)\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(OrderLine.order)\
        .filter(Order.refdate == refdate)\
        .filter(OrderLine.quantity_ordered > 0)\
        .filter(factory_alias.shortname == factoryname)\
        .order_by(ProductSubcategory.category_name, Product.subcategory_name, Product.shortname)
    if product_categorynames:
        q3 = q3.filter(ProductSubcategory.category_name.in_(product_categorynames))
    lines = []
    # combine all in a matrix
    for product in q3:
        total_quantity = 0
        item = {'product': product}
        quantities = []
        for cl in clients:
            qty = subq[cl].get(product.reference, 0)
            quantities.append(qty)
            total_quantity += qty
        # insert *now* the first column that holds the total quantity (excluding factory's own orders)
        if factory_is_client:
            quantities.insert(0, total_quantity - subq[factoryname].get(product.reference, 0))
        item['total'] = total_quantity
        item['quantities'] = quantities
        lines.append(item)
    
    if factory_is_client:
        clients.insert(0, 'Hors {}'.format(factoryname))
    
    return clients, lines



def _list_custom_products_planning(factoryname, refdate, daysmax=31, dbsession=None):
    '''Retrieve the list of future special orders.'''
    if not dbsession:
        dbsession = get_current_request().dbsession
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    now = datetime.now()
    datemax = now + timedelta(days=daysmax)
    q = dbsession.query(OrderLineCustomProduct)\
        .join(OrderLineCustomProduct.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(OrderLineCustomProduct.order)\
        .filter(factory_alias.shortname == factoryname)\
        .filter(Order.due_date <= datemax)\
        .filter(Order.due_date > now)
    return q.all()


def custom_products_for_production_center(factoryname, refdate, clientnames=[], dbsession=None):
    if not dbsession:
        dbsession = get_current_request().dbsession
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    refdate = util.normalize_refdate(refdate)
    q = dbsession.query(OrderLineCustomProduct)\
        .join(OrderLineCustomProduct.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(OrderLineCustomProduct.order)\
        .filter(OrderLineCustomProduct.quantity_ordered > 0)\
        .filter(factory_alias.shortname == factoryname)\
        .filter(Order.refdate == refdate)
    if clientnames:
        q = q.join(client_alias, Product.clients)\
            .filter(client_alias.shortname.in_(clientnames))
    return q.all()
    


def fetch_delivery_plan(factoryname, refdate='tomorrow', dbsession=None):
    # XXX this function has a lot of code in common with fetch_prod_todolist() above. TODO DRY
    if not dbsession:
        dbsession = get_current_request().dbsession
    refdate = util.normalize_refdate(refdate)
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    # list of clients that have ordered something from the current factory
    clients = [wp[0] for wp in dbsession.query(client_alias.shortname)\
        .select_from(OrderLine)\
        .join(OrderLine.order)\
        .join(client_alias, Order.client)\
        .join(OrderLine.product)\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)
        .filter(Order.refdate == refdate)\
        .filter(factory_alias.shortname == factoryname)\
        .filter(sa.or_(OrderLine.quantity_produced > 0, OrderLine.quantity_ordered > 0))\
        .group_by(client_alias.shortname)\
        .all()]
    
    # Make sure we include clients who have only ordered custom products
    q1bis = dbsession.query(client_alias.shortname)\
        .select_from(OrderLineCustomProduct)\
        .join(OrderLineCustomProduct.order)\
        .join(client_alias, Order.client)\
        .join(OrderLineCustomProduct.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .filter(Order.refdate == refdate)\
        .filter(factory_alias.shortname == factoryname)\
        .filter(sa.or_(OrderLine.quantity_produced > 0, OrderLine.quantity_ordered > 0))\
        .group_by(client_alias.shortname)
    for row in q1bis:
        clname = row[0]
        if clname not in clients:
            clients.append(clname)
    clients = sorted(clients)
    
    # the factory don't need delivery for herself
    if factoryname in clients:
        clients.pop(clients.index(factoryname))
    # quantity of each product ordered by each client
    subq = OrderedDict()
    for cl in clients:
        subq[cl] = {ref: (qtyp, qtyo) for ref, qtyp, qtyo in dbsession.query(Product.reference, sa.func.sum(OrderLine.quantity_produced), sa.func.sum(OrderLine.quantity_ordered))\
            .select_from(OrderLine)\
            .join(OrderLine.product)\
            .join(Product.subcategory)\
            .join(ProductSubcategory.category)\
            .join(factory_alias, ProductCategory.production_center)\
            .join(OrderLine.order)\
            .join(client_alias, Order.client)\
            .filter(Order.refdate == refdate)\
            .filter(client_alias.shortname == cl)\
            .filter(sa.or_(OrderLine.quantity_produced > 0, OrderLine.quantity_ordered > 0))\
            .filter(factory_alias.shortname == factoryname)\
            .group_by(Product.reference)}
    # log.debug(subq)
    # products available in the current factory
    q3 = dbsession.query(Product)\
        .select_from(OrderLine)\
        .join(OrderLine.product)\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(OrderLine.order)\
        .filter(Order.refdate == refdate)\
        .filter(sa.or_(OrderLine.quantity_produced > 0, OrderLine.quantity_ordered > 0))\
        .filter(factory_alias.shortname == factoryname)\
        .order_by(ProductSubcategory.category_name, Product.subcategory_name, Product.shortname)
    
    # combine all in a matrix
    # Custom products come first
    lines = []
    for cp in custom_products_for_production_center(factoryname, refdate, clients, dbsession=dbsession):
        total_quantity = 0
        item = {
            "product": {
                "subcategory_name": cp.category_name,
                "shortname": cp.display_label(),
                "reference": "???"
            },
            "quantities": []
        }
        for cl in clients:
            if cp.order.client.shortname == cl:
                item["quantities"].append(1)
                total_quantity += 1
            else:
                item["quantities"].append(0)
        item['total'] = total_quantity
        lines.append(item)
    # Normal products come after
    for product in q3:
        total_quantity = 0
        item = {'product': product}
        quantities = []
        for cl in clients:
            qtyp, qtyo = subq[cl].get(product.reference, (0, 0))
            qty = qtyp or qtyo or 0
            quantities.append(qty)
            total_quantity += qty
        item['total'] = total_quantity
        item['quantities'] = quantities
        lines.append(item)
    return clients, lines

def fetch_delivery_plan_simplified(factoryname, refdate='tomorrow', dbsession=None):
    '''Simplified delivery plan only lists products with quantity > 0.'''
    if not dbsession:
        dbsession = get_current_request().dbsession
    refdate = util.normalize_refdate(refdate)
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    
    items_by_destination = OrderedDict()
    # Custom products come first
    for cp in custom_products_for_production_center(factoryname, refdate, dbsession=dbsession):
        dest = cp.order.client.shortname
        if dest not in items_by_destination:
            items_by_destination[dest] = []
        items_by_destination[dest].append({
            "product": {
                "shortname": cp.display_label(),
            },
            "quantity_ordered": cp.quantity_ordered,
            "quantity_produced": cp.quantity_produced,
        })
        
    q = dbsession.query(OrderLine)\
        .options(joinedload(OrderLine.product))\
        .join(OrderLine.product)\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(OrderLine.order)\
        .join(client_alias, Order.client)\
        .filter(Order.refdate == refdate)\
        .filter(sa.or_(OrderLine.quantity_produced > 0, OrderLine.quantity_ordered > 0))\
        .filter(factory_alias.shortname == factoryname)
    r = q.all()
    # Normal products come after
    for item in r:
        dest = item.order.client.shortname
        if dest not in items_by_destination:
            items_by_destination[dest] = []
        items_by_destination[dest].append(item)
    return items_by_destination

def fetch_prod_todolists_by_dates(factoryname, refdate='tomorrow', daycount=7, dbsession=None):
    if not dbsession:
        dbsession = get_current_request().dbsession
    factory_alias = aliased(Workplace)
    # list of refdates to return
    refdates = util.refdates_range(refdate, daycount)
    # quantity of each product ordered by day
    subq = OrderedDict()
    for date in refdates:
        subq[date] = {ref: qty for ref, qty in dbsession.query(Product.reference, sa.func.sum(OrderLine.quantity_ordered))\
            .select_from(OrderLine)\
            .join(OrderLine.product)\
            .join(Product.subcategory)\
            .join(ProductSubcategory.category)\
            .join(factory_alias, ProductCategory.production_center)\
            .join(OrderLine.order)\
            .filter(Order.refdate == date)\
            .filter(OrderLine.quantity_ordered > 0)\
            .filter(factory_alias.shortname == factoryname)\
            .group_by(Product.reference)}
    # products available in the current factory
    q = dbsession.query(Product.reference, Product.shortname)\
        .select_from(OrderLine)\
        .join(OrderLine.product)\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(OrderLine.order)\
        .join(Product.subcategory)\
        .filter(Order.refdate.between(refdates[0], refdates[-1]))\
        .filter(OrderLine.quantity_ordered > 0)\
        .filter(factory_alias.shortname == factoryname)\
        .order_by(ProductSubcategory.category_name, Product.subcategory_name, Product.shortname)\
        .group_by(Product.reference, Product.shortname, ProductSubcategory.category_name, Product.subcategory_name)
    # combine all in a matrix
    lines = []
    for product in q:
        log.debug(product)
        total_quantity = 0
        item = {'product': product}
        quantities = []
        for date in refdates:
            qty = subq[date].get(product.reference, 0)
            quantities.append(qty)
            total_quantity += qty
        item['total'] = total_quantity
        item['quantities'] = quantities
        lines.append(item)
    return refdates, lines


def fetch_invoice_data(factoryname, clientname, refmonth='last', dbsession=None):
    if not dbsession:
        dbsession = get_current_request().dbsession
    
    # date range
    dt_start, dt_end = util.refmonth_daterange(refmonth)
    refmonth = util.normalize_refmonth(refmonth)
    factory_alias = aliased(Workplace)
    client_alias = aliased(Workplace)
    refmonth_fmt = dt_start.strftime('%Y-%m')
    
    # workplaces info
    client = dbsession.query(Workplace).filter(Workplace.shortname==clientname).first()
    seller = dbsession.query(Workplace).filter(Workplace.shortname==factoryname).first()
    
    # quantity of each product ordered by each client
    subq_cl = {}
    q2 = dbsession.query(Product.reference, sa.func.sum(OrderLine.quantity_ordered))\
        .select_from(OrderLine)\
        .join(OrderLine.product)\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(OrderLine.order)\
        .join(client_alias, Order.client)\
        .filter(Order.refdate.like(refmonth + '%'))\
        .filter(client_alias.shortname == clientname)\
        .filter(OrderLine.quantity_ordered > 0)\
        .filter(factory_alias.shortname == factoryname)\
        .group_by(Product.reference)
    subq_cl = {ref: qty for ref, qty in q2}
    
    # details of products
    q3 = dbsession.query(Product)\
        .options(joinedload(Product.subcategory).joinedload(ProductSubcategory.category))\
        .join(Product.subcategory)\
        .join(ProductSubcategory.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .join(Product.subcategory)\
        .filter(factory_alias.shortname == factoryname)\
        .order_by(ProductSubcategory.category_name, Product.subcategory_name, Product.shortname)
    
    # combine data
    total_excl_vat = 0
    total_vat_amount = 0
    total_incl_vat = 0
    items = []
    vat_rates = OrderedDict()
    
    # Custom products come first
    config_manager = EclairConfigManager(dbsession)
    internal_price_ratio = config_manager.get_float('internal_price_conversion_ratio', 55)
    q4 = dbsession.query(OrderLineCustomProduct)\
        .join(OrderLineCustomProduct.order)\
        .join(client_alias, Order.client)\
        .join(OrderLineCustomProduct.category)\
        .join(factory_alias, ProductCategory.production_center)\
        .filter(factory_alias.shortname == factoryname)\
        .filter(Order.refdate.like(refmonth + '%'))\
        .filter(client_alias.shortname == clientname)
    for cp in q4:
        unit_price = cp.price * internal_price_ratio / 100
        vat_rate = cp.category.vatrate or 5.5 # XXX hard-coded fallback value
        if vat_rate not in vat_rates:
            vat_rates[vat_rate] = chr(ord('A') + len(vat_rates))
        vat_amount = unit_price * vat_rate / 100
        price_incl_vat = unit_price + vat_amount
        item = {
            "category": cp.category_name,
            "name": cp.productname,
            "quantity": 1,
            "unit_price": unit_price,
            "vat_rate": vat_rates.get(vat_rate),
            "vat_amount": vat_amount,
            "price_excl_vat": unit_price,
            "price_incl_vat": price_incl_vat
        }
        items.append(item)
        total_excl_vat += item['price_excl_vat']
        total_vat_amount += item['vat_amount']
        total_incl_vat += item['price_incl_vat']
    
    # "Normal products"
    for product in q3:
        quantity = subq_cl.get(product.reference, 0)
        if not quantity:
            continue
        unit_price = product.price or 0
        vat_rate = product.subcategory.category.vatrate or 5.5 # XXX hard-coded fallback value
        if vat_rate not in vat_rates:
            vat_rates[vat_rate] = chr(ord('A') + len(vat_rates))
        price_excl_vat = quantity * unit_price
        vat_amount = price_excl_vat * vat_rate / 100
        price_incl_vat = price_excl_vat + vat_amount
        
        item = {
            "category": product.subcategory.category_name + " - " + product.subcategory_name,
            "name": product.shortname,
            "quantity": quantity,
            "unit_price": unit_price,
            "vat_rate": vat_rates.get(vat_rate),
            "vat_amount": vat_amount,
            "price_excl_vat": price_excl_vat,
            "price_incl_vat": price_incl_vat
        }
        items.append(item)
        total_excl_vat += item['price_excl_vat']
        total_vat_amount += item['vat_amount']
        total_incl_vat += item['price_incl_vat']
    
    return {
        "date": dt_end,
        "reference": "VI-{}-{}-{}".format(refmonth_fmt, factoryname, clientname),
        "client": client.legal_info(),
        "seller": seller.legal_info(),
        "total_excl_vat": total_excl_vat,
        "total_incl_vat": total_incl_vat,
        "total_vat_amount": total_vat_amount,
        "items": items,
        "vat_rates": {v: k for k, v in vat_rates.items()},
    }


def clone_order(clientname, refdate_src, refdate_dst, dbsession=None):
    '''Replicate an order from one date to another.'''
    if not dbsession:
        dbsession = get_current_request().dbsession
    log.debug("Cloning order of {} from {} into {}".format(clientname, refdate_src, refdate_dst))
    
    src_order, src_items, src_total_price = fetch_order_data(clientname, refdate_src, dbsession)
    dst_order = get_order_and_create_it_if_needed(clientname, refdate_dst, dbsession)
    
    # Completely delete existing items
    qd = OrderLine.__table__.delete()\
        .where(OrderLine.__table__.c.order_id == dst_order.id)
    dbsession.execute(qd)
    
    # Now copy each item from src order to dst order
    for product_id, item in src_items.items():
        if item.get('quantity_ordered', 0) > 0:
            newitem = OrderLine()
            newitem.order_id = dst_order.id
            newitem.quantity_ordered = item.get('quantity_ordered')
            newitem.product_id = product_id
            dbsession.add(newitem)
