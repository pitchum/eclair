def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('route_path', '/__route_path__')
    
    config.add_route('users', '/users')
    config.add_route('user',  '/user/{username}')
    config.add_route('roles', '/roles')
    config.add_route('role',  '/role/{rolename}')
    config.add_route('user_role',  '/user_role')
    config.add_route('role_permissions',  '/role_permissions')
    config.add_route('config',  '/configuration')
    
    config.add_route('workplaces',  '/workplaces')
    config.add_route('workplace',   '/workplace/{shortname}')
    
    config.add_route('home',   '/')
    config.add_route('login',  '/login')
    config.add_route('logout', '/logout')
    
    config.add_route('dashboard', '/dashboard')
    
    config.add_route('products.byfactory', '/products/{factoryname}')
    config.add_route('products', '/products/')
    config.add_route('product',  '/product/{reference}')
    config.add_route('product_categories',  '/categories/')
    config.add_route('product_category',    '/category/{categoryname}')
    config.add_route('product_subcategory', '/subcategory/{subcategoryname}')
    config.add_route('product.generate_ref', '/productreference/generate')
    
    config.add_route('order.clone', '/order/clone')
    config.add_route('orders', '/orders/{clientname}/{refdate}')
    config.add_route('order',  '/order/{clientname}/{refdate}')
    config.add_route('orderlines', '/orderlines/{clientname}/{refdate}')
    
    config.add_route('orderlinecustomproducts',  '/orderlinecustomproducts/{clientname}/{refdate}/')
    config.add_route('orderlinecustomproduct.add',  '/order/{clientname}/{refdate}/addcustomline')
    config.add_route('orderlinecustomproduct',  '/orderlinecustomproduct/{id}')
    
    config.add_route('production_plan',  '/prod/{factoryname}/{refdate}')
    config.add_route('production_plans', '/prods/{factoryname}/{refdate}')
    
    config.add_route('delivery',             '/delivery/{factoryname}/{refdate}')
    
    config.add_route('invoices', '/invoices/{factoryname}/')
    config.add_route('invoice',  '/invoice/{factoryname}/{refmonth}/{clientname}')
