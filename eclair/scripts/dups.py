import logging
logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)-7.7s %(module)s.py:%(lineno)s \t%(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
log = logging.getLogger(__name__)


from .. import models as mdl
import sqlalchemy as sa
from .cli import open_dbsession


def _dedup_parser(subparsers):
    '''Outil de recherche de doublons dans les commandes (outil temporaire/jetable).'''
    parser = subparsers.add_parser("dups", help="Manage duplicates in orderlines")
    subcmd_show = parser.add_subparsers(dest='dups_action')
    dups_show_parser = subcmd_show.add_parser('list', help='Print the current dups.')
    
    dups_solve_parser = subcmd_show.add_parser('solve', help='Choose a solution for a given dup issue.')
    dups_solve_parser.add_argument('solution', choices=['sum', 'first', 'set'], help="""Solution type.\n
'sum' will merge duplicates by summing their values, thereby accepting the quantity seen by the production center.\n
'first' will keep the first quantity in the list and remove the others. This is equivalent of accepting the quantity seen by the client.\n
'set' will delete dups and set a new value (see next option).""")
    dups_solve_parser.add_argument('dup_id', help="The id of the duplicate as given by the 'list' command. Special value 'all' will apply the solution to all duplicates without prompting.")
    dups_solve_parser.add_argument('new_quantity', nargs='?', help="The new quantity to store in DB (only useful with solution=set, see option above).")

    dups_report_parser = subcmd_show.add_parser('report', help='Print (in human-readable French) a reporting of existing duplicates.')


def main(args):
    if args.verbose:
        log.setLevel(logging.DEBUG)
    if args.dups_action is None:
        log.error("Missing argument")
        return 1
    
    from ..util import dbapi
    dbapi.log.setLevel(log.level)
    sess = open_dbsession(args.configfile)
    solver = DupSolver(sess)
    
    if args.dups_action == 'list':
        for dup in solver._iter_duplicates(limit=500):
            print("{}-{}".format(dup.order_id, dup.product_id))
    elif args.dups_action == 'solve':
        if args.dup_id == 'all':
            for dup in solver._iter_duplicates(limit=500):
                solver._solve_dup_by_summing(dup.order_id, dup.product_id)
                print()
            return
        order_id, product_id = args.dup_id.split('-')
        solver._solve_dup_by_summing(order_id, product_id)
    elif args.dups_action == 'report':
        solver.report()
    else:
        log.error("Unsupported action: '{}'".format(args.dups_action))



class DupSolver(object):
    
    def __init__(self, sess):
        self.sess = sess


    def _iter_duplicates(self, limit=10):
        dupcount = (sa.func.count(mdl.OrderLine.id))
        q = self.sess.query(mdl.OrderLine.order_id, mdl.OrderLine.product_id, dupcount)\
            .select_from(mdl.OrderLine)\
            .group_by(mdl.OrderLine.order_id, mdl.OrderLine.product_id)\
            .having(dupcount > 1)
        if limit is not None:
            q = q.limit(limit)
        for row in q:
            yield row


    def _solve_dup_by_summing(self, order_id, product_id):
        q = self.sess.query(mdl.OrderLine)\
            .options(sa.orm.joinedload(mdl.OrderLine.order))\
            .filter(mdl.OrderLine.order_id == order_id)\
            .filter(mdl.OrderLine.product_id == product_id)
        dups = q.all()
        if len(dups) < 2:
            raise Exception("WTF?!? No dups found!")
        row_to_keep = dups[0]
        first = row_to_keep
        print("-- https://eclair.maisonlandemaine.com/order/{}/{} (prod: {})\n-- Merging {} duplicates by summing quantities (to get the quantity actually produced and delivered)."\
            .format(first.order.client.shortname, first.order.refdate, first.product.subcategory.category.production_center.shortname, len(dups)))
        for candidate in dups:
            print("--  -> " + self.pprint(candidate))
        final_qty = row_to_keep.quantity_ordered
        for dup in dups[1:]:
            final_qty += dup.quantity_ordered
            log.debug("delete {}".format(self.pprint(dup)))
            print("DELETE FROM orderline WHERE id = {};".format(dup.id))
        log.debug("update {}, set {}".format(dup.id, final_qty))
        print("UPDATE orderline SET quantity_ordered = {} WHERE id = {};".format(final_qty, row_to_keep.id))


    def pprint(self, ol):
        return "line #{}. '{}' x {}"\
            .format(ol.id, ol.product.shortname, ol.quantity_ordered)

    
    def _report(self, order_id, product_id):
        q = self.sess.query(mdl.OrderLine)\
            .options(sa.orm.joinedload(mdl.OrderLine.order))\
            .options(sa.orm.joinedload(mdl.OrderLine.product))\
            .filter(mdl.OrderLine.order_id == order_id)\
            .filter(mdl.OrderLine.product_id == product_id)
        dups = q.all()
        if len(dups) < 2:
            raise Exception("WTF?!? No dups found!")
        row_to_keep = dups[0]
        first = row_to_keep
        final_qty = row_to_keep.quantity_ordered
        for dup in dups[1:]:
            final_qty += dup.quantity_ordered
        hr = first.order.refdate
        print("""- https://eclair.maisonlandemaine.com/order/{clname}/{refdate} : le {refdate_hr}, {prodcenter} a facturé {qty_real} {prodname} alors que {clname} pensait en avoir commandé {qty_wrong}."""\
            .format(**{
                "clname": first.order.client.shortname,
                "refdate": first.order.refdate,
                "refdate_hr": hr[0:4] + '-' + hr[4:6] + '-' + hr[6:],
                "prodcenter": first.product.subcategory.category.production_center.shortname,
                "prodname": first.product.shortname,
                "qty_real": final_qty,
                "qty_wrong": first.quantity_ordered,
            }))
    
    def report(self):
        for dup in self._iter_duplicates(limit=500):
            self._report(dup.order_id, dup.product_id)
            print
            
            
