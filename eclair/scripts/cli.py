import logging
logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)-7.7s %(module)s.py:%(lineno)s \t%(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
log = logging.getLogger(__name__)
from argparse import ArgumentParser
import os
from datetime import datetime

from ..util.csvimporter import CsvCatalogueImporter
from ..models.meta import EclairBaseModel
from ..models import User, Role, RolePermission

from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func

def main():
    log.debug("CLI called.")
    args = _parse_args()
    log.debug(args)
    
    if args.action == 'db':
        if args.db_action == 'csv-import':
            from ..util.csvimporter import log as _log
            _log.setLevel(log.level)
            log.info("importing data from CSV files")
            sess = open_dbsession(args.configfile)
            reader = CsvCatalogueImporter(sess, dry_run=args.dry_run)
            for filename in args.filenames:
                log.debug("importing {}".format(filename))
                reader.import_products_from_file(filename)
            sess.commit()
        elif args.db_action.startswith('ods'):
            from ..util.odsimporter import OdsImporter, log as _log
            _log.setLevel(log.level)
            sess = open_dbsession(args.configfile)
            reader = OdsImporter(sess, dry_run=args.dry_run)
            if args.db_action == 'odsimport':
                reader.import_files(args.filenames)
            elif args.db_action == 'odsanalyze':
                res = reader.analyze(args.filenames)
                print(res)
            sess.commit()
        elif args.db_action == 'diagnose':
            sess = open_dbsession(args.configfile)
            rolecount = sess.query(func.count(Role.rolename)).scalar()
            if rolecount < 5:
                if rolecount == 0:
                    log.error('Table role is empty')
                else:
                    log.warning("Only {} rows in table 'role'.".format(rolecount))
            # Inconsistency check: products should not be associated with workplaces that are not tagged as factories
            q = sess.query(Workplace).join(Workplace.products).filter(Workplace.factory != True)
            for wp in q.all():
                log.warning("Workplace '{}' is not flagged as a factory but has {} products associated.".format(wp.shortname, len(wp.products)))
            sess.commit()
        elif args.db_action == 'reset-superadmin':
            sess = open_dbsession(args.configfile)
            su_role = sess.query(Role).get('superadmin')
            if not su_role:
                su_role = Role(rolename='superadmin')
                sess.add(su_role)
            su_account = sess.query(User).get(args.username)
            if not su_account:
                su_account = User()
                sess.add(su_account)
            su_account.username = args.username
            su_account.set_password(args.password)
            su_account.roles.append(su_role)
            sess.merge(su_account)
            # give all permission to superadmin role
            from ..security import permissions
            for permname in permissions:
                rp = RolePermission(rolename='superadmin', permname=permname)
                sess.merge(rp)
            sess.commit()
        elif args.db_action == 'setup-default-roles':
            sess = open_dbsession(args.configfile)
            for rolename in ('seller', 'producer', 'carrier', 'admin'):
                r = Role(rolename=rolename)
                # Every user should be granted permission 'order:view'.
                rp = RolePermission(rolename=r.rolename, permname='order:view')
                sess.merge(r)
                sess.merge(rp)
            rp = RolePermission(rolename='seller', permname='order:modify')
            sess.merge(rp)
            sess.commit()
        elif args.db_action == 'populate-demo':
            # TODO detect if db exists and/or has valuable data
            sess = open_dbsession(args.configfile)
            
            sess.commit()
        elif args.db_action == 'todolist':
            from ..util import dbapi
            dbapi.log.setLevel(log.level)
            sess = open_dbsession(args.configfile)
            clients, lines = dbapi.fetch_prod_todolist(args.factoryshortname, dbsession=sess)
            print(lines)
        elif args.db_action == 'clone-order':
            from ..util import dbapi
            dbapi.log.setLevel(log.level)
            sess = open_dbsession(args.configfile)
            dbapi.clone_order(args.clientname, args.refdate_src, args.refdate_dst, sess)
            sess.commit()
    elif args.action == 'production':
        if args.production_action is None:
            log.error("Missing argument")
        elif args.production_action == 'show':
            from ..util import dbapi
            import json
            dbapi.log.setLevel(log.level)
            sess = open_dbsession(args.configfile)
            clientnames, lines = dbapi.fetch_prod_todolist(args.factoryshortname, args.refdate, dbsession=sess)
            print("Clients: {}".format(clientnames))
            for line in lines:
                print(json.dumps(line, default=str))
        else:
            log.error("Unsupported action: '{}'".format(args.production_action))
    elif args.action == 'delivery':
        if args.delivery_action is None:
            log.error("Missing argument")
        elif args.delivery_action == 'show':
            from ..util import dbapi
            import json
            dbapi.log.setLevel(log.level)
            sess = open_dbsession(args.configfile)
            clientnames, lines = dbapi.fetch_delivery_plan(args.factoryshortname, args.refdate, sess)
            print("Clients: {}".format(clientnames))
            for line in lines:
                print(json.dumps(line, default=str))
        else:
            log.error("Unsupported action: '{}'".format(args.delivery_action))
    elif args.action == 'dups':
        from .dups import main
        main(args)


def _parse_args():
    parser = ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', help="Print debug messages")
    parser.add_argument('-n', '--dry-run', action='store_true', help="Don't really apply modifications, just simulate.")
    parser.add_argument('-c', '--config', dest="configfile", default="eclair.ini", metavar="eclair.ini", help="Configuration file.")
    subparsers = parser.add_subparsers(dest='action')
    
    _production_parser(subparsers)
    _delivery_parser(subparsers)
    from .dups import _dedup_parser
    _dedup_parser(subparsers)
    
    parser_db = subparsers.add_parser("db", help="Manage eclair database content.")
    db_subparsers = parser_db.add_subparsers(dest='db_action')

    # eclair-cli db csv-import
    parser_db_import_csv = db_subparsers.add_parser('csv-import', help='Populate DB with CSV dataset (developer only)')
    parser_db_import_csv.add_argument(dest="filenames", nargs='+', default=".", help="CSV files to import.")
    
    # odsimport
    parser_db_import_ods = db_subparsers.add_parser('odsimport', help='Populate DB with ODS dataset (developer only)')
    parser_db_import_ods.add_argument(dest="filenames", nargs='+', default=".", help="ODS files to import.")
    parser_db_analyze_ods = db_subparsers.add_parser('odsanalyze', help='Analyze ODS dataset (developer only)')
    parser_db_analyze_ods.add_argument(dest="filenames", nargs='+', default=".", help="ODS files to analyze.")

    # eclair-cli db diagnose
    parser_db_diag = db_subparsers.add_parser('diagnose', help='Performs sanity checks.')
    
    # setup-default-roles
    setup_default_roles = db_subparsers.add_parser('setup-default-roles', help="Creates default user roles with minimal default permission.")
    
    # reset-superadmin
    reset_superadmin = db_subparsers.add_parser('reset-superadmin', help="Resets superadmin account.")
    reset_superadmin.add_argument('-u', '--username', dest='username', default='superadmin', )
    reset_superadmin.add_argument('-p', '--password', dest='password', default='superadmin')
    
    # populate-demo
    parser_db_populate_demo = db_subparsers.add_parser('populate-demo', help='Initilize DB with demo data.')
    parser_db_populate_demo.add_argument('-f', '--force', dest='force', action='store_true', help="Remove existing DB if present.")
    
    # test
    parser_test = db_subparsers.add_parser('todolist', help='Print a todolist')
    parser_test.add_argument('factoryshortname')
    
    # clone-order
    parser_clone = db_subparsers.add_parser('clone-order', help='Test cloning an order.')
    parser_clone.add_argument(dest='clientname')
    parser_clone.add_argument(dest='refdate_src')
    parser_clone.add_argument(dest='refdate_dst')
    
    args = parser.parse_args()
    if args.verbose:
        log.setLevel(logging.DEBUG)
    
    return args

def _production_parser(subparsers):
    '''Parser options specific to the 'production' subcommand.'''
    parser = subparsers.add_parser("production", help="Manage production plans")
    subcmd_show = parser.add_subparsers(dest='production_action')
    produciton_show_parser = subcmd_show.add_parser('show', help='Print the content of a given production plan')
    produciton_show_parser.add_argument('factoryshortname')
    produciton_show_parser.add_argument('refdate', nargs='?', default='today')


def _delivery_parser(subparsers):
    '''Parser options specific to the 'delivery' subcommand.'''
    parser = subparsers.add_parser("delivery", help="Manage delivery plans")
    subcmd_show = parser.add_subparsers(dest='delivery_action')
    devivery_show_parser = subcmd_show.add_parser('show', help='Print the content of a given delivery plan')
    devivery_show_parser.add_argument('factoryshortname')
    devivery_show_parser.add_argument('refdate', nargs='?', default='today')


def get_config(configfile=None):
    '''Returns a ``ConfigParser`` object.'''
    possible_configfiles = [
        './eclair.ini',
        '~/.config/eclair.ini',
        '/etc/eclair.ini',
        '/etc/eclair/eclair.ini',
        './development.ini',
    ]
    
    if configfile is None or not os.path.isfile(configfile):
        for candidate in possible_configfiles:
            candidate = os.path.expanduser(candidate)
            if os.path.isfile(candidate):
                configfile = candidate
                log.debug("Found configuration file %s." % configfile)
                break
    
    if configfile is None:
        raise Exception("No configuration file found. Either use -c option of create one these files: %s" % ",".join(possible_configfiles))

    try:
        from ConfigParser import ConfigParser # python2
    except:
        from configparser import ConfigParser # python3
    
    conf = ConfigParser()
    conf.read(configfile)
    return conf


def open_dbsession(configfile=None, section='app:main', **kwargs):
    '''
    Reads SQLAlchemy configuration parameters from the given configuration file
    and creates a new session.
    '''
    engine = create_engine(configfile, section, 'sqlalchemy.')
    DBSession = sessionmaker()
    DBSession.configure(bind=engine)
    EclairBaseModel.metadata.bind = engine
    return DBSession()


def create_engine(configfile=None, section='app:main', prefix='sqlalchemy.'):
    pdir = os.path.dirname(os.path.abspath(configfile))
    settings = dict(get_config(configfile).items(section, vars={'here': pdir}))
    engine = engine_from_config(settings, prefix)
    return engine
