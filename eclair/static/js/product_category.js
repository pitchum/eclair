
function addUser(evt) {
    var categoryname = evt.target.closest('[data-categoryname]').dataset.categoryname;
    var username = evt.target.closest('div').querySelector('input[type=text]').value;
    app.dbapi.addUserToProductCategory(categoryname, [username], function(response) {
        if (response.success == true) {
            location.reload(); // XXX could do better
        } else {
            var msg = "";
            for (var i = 0; i < response.errors.length; i++) {
                msg += response.errors[i] + "\n";
            }
            alert(msg);
        }
    });
}

function removeUser(evt) {
    var categoryname = evt.target.closest('[data-categoryname]').dataset.categoryname;
    var username = evt.target.closest('[data-username]').dataset.username;
    app.dbapi.removeUserFromProductCategory(categoryname, [username], function(response) {
        if (response.success == true) {
            location.reload(); // XXX could do better
        } else {
            var msg = "";
            for (var i = 0; i < response.errors.length; i++) {
                msg += response.errors[i] + "\n";
            }
            alert(msg);
        }
    });
}

function addSubCategory(evt) {
    var input = evt.target.closest('div').querySelector('input[type=text]');
    // Sanity check
    var regex = RegExp('^' + input.getAttribute('pattern') + '$');
    var subcategoryname = input.value;
    if (!regex.test(subcategoryname)) {
        alert(input.getAttribute('title'));
        return;
    }
    
    var categoryname = evt.target.closest('[data-categoryname]').dataset.categoryname;
    app.dbapi.createProductSubategory(categoryname, [subcategoryname], function(response) {
        if (response.success == true) {
            location.reload(); // XXX could do better
        } else {
            var msg = "";
            for (var i = 0; i < response.errors.length; i++) {
                msg += response.errors[i] + "\n";
            }
            alert(msg);
        }
    });
}

if (document.querySelector('#btn-add-user')) {
    document.querySelector('#btn-add-user').addEventListener('click', addUser);
    document.querySelectorAll('.btn-remove-user-product-category').forEach(function (btn) {
        btn.addEventListener('click', removeUser);
    });
}
if (document.querySelector('#btn-create-subcategory')) {
    document.querySelector('#btn-create-subcategory').addEventListener('click', addSubCategory);
}
