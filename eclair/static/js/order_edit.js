//var clientname = document.querySelector('[data-clientname]').dataset.clientname;
//var refdate = document.querySelector('[data-refdate]').dataset.refdate;


function update_orderline(evt) {
    var tr = evt.target.closest('tr');
    if (evt.target.tagName != 'INPUT') {
        var btn = evt.target;
        var inc = true;
        if (btn.classList.contains('btn-decrease')) {
            inc = false;
        }
        var qty_holder = tr.querySelector('input');
        if (inc) {
            qty_holder.value = Number(qty_holder.value) + 1;
        } else {
            qty_holder.value = Number(qty_holder.value) - 1;
        }
    }
    updateQuantity(tr);
}

function updateQuantity(ele) {
    var tr = ele.closest('tr');
    var orderContainer = document.querySelector('#order-view');
    var _clientname = orderContainer.dataset.clientname;
    var _refdate = orderContainer.dataset.refdate;
    var qty_holder = tr.querySelector('input');
    var qty = Number(qty_holder.value);
    // mark the input as stale
    qty_holder.classList.add('stale');

    app.orderapi.enqueue({
        "clientname": _clientname,
        "refdate": _refdate,
        "product_id": tr.dataset.productId,
        "input": qty_holder,
    });

}


function changeVal(increase) {
    var ele = document.activeElement;
    if (ele.tagName == 'INPUT' && ele.classList.contains('quantity')) {
        var curVal = Number(ele.value);
        if (increase) {
            ele.value = ++curVal;
        } else {
            if (curVal > 0) {
              ele.value = --curVal;
            }
        }
        updateQuantity(ele);
    }
}


function gotoLine(goUpward) {
    var moveUp = false;
    if (goUpward) {
        moveUp = true;
    }
    var currentLine = document.activeElement;
    if (currentLine.tagName == 'INPUT') {
        var tgtRow ;
        if (moveUp) {
            tgtRow = currentLine.closest('tr').previousElementSibling;
        } else {
            tgtRow = currentLine.closest('tr').nextElementSibling;
        }
        if (tgtRow) {
            tgtRow.querySelector('input.quantity').select();
        }
    } else {
        document.querySelector('input.quantity').select();
    }
    // TODO scroll page to make the selected row visible
}


function keyboardShortCut(evt) {
    if (evt.defaultPrevented) {
        return; // Do nothing if the event was already processed
    }
    if (evt.ctrlKey) {
        switch (evt.key) {
            case "Control":
                // Always triggered so catching it here otherwise lots of
                // "natural" browser shortcuts would break.
            break;
            case "ArrowDown":
                evt.preventDefault(); // XXX not nice for people who like to scroll page using keyboard
                gotoLine();
            break;
            case "ArrowUp":
                evt.preventDefault(); // XXX not nice for people who like to scroll page using keyboard
                gotoLine(true);
            break;
            case "+":
                evt.preventDefault(); // XXX not nice for people who like to scroll page using keyboard
                changeVal(true);
            break;
            case "ArrowRight":
                evt.preventDefault(); // XXX not nice for people who like to scroll page using keyboard
                changeVal(true);
            break;
            case "-":
                evt.preventDefault(); // XXX not nice for people who like to scroll page using keyboard
                changeVal(false);
            break;
            case "ArrowLeft":
                evt.preventDefault(); // XXX not nice for people who like to scroll page using keyboard
                changeVal(false);
            break;
            default:
                console.log("Unhandled keyboard shortcut: Ctrl+" + evt.key);
        }
    }
}


document.addEventListener('keydown', keyboardShortCut, false);

document.querySelectorAll('input.quantity').forEach(function (input) {
    input.addEventListener('change', update_orderline);
});
document.querySelectorAll('.btn-increase, .btn-decrease').forEach(function (btn) {
    btn.addEventListener('click', update_orderline);
});
document.querySelectorAll('input[type=number]').forEach(function (btn) {
    btn.addEventListener('click', function(evt) {
        evt.target.select();
    });
});
