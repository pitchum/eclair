/* auto-refresh data when order-lock-time is reached */
if (document.querySelector('[data-lock-delay]')) {
    var lock_delay = 1 + Number(document.querySelector('[data-lock-delay]').dataset.lockDelay);
    console.log("Page will be auto-reloaded in", lock_delay, "seconds");
    setTimeout(location.reload.bind(location), lock_delay * 1000);
}
