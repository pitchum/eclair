
function createNewUser() {
    var username = document.querySelector('#create-user input').value;
    if (!username || username.length < 3) {
        console.error("invalid username");
        alert("Identifiant non valide !");
        return false;
    }
    var url = route_path('users');
    eclairAjax({
        url: url,
        data: JSON.stringify({username: username}),
        success: function(response) {
            if (response.success) {
                window.location.href = route_path('user', {username: response.user.username});
            } else {
                console.error(response);
                alert("L'utilisateur n'a pas pû être créé.\n\n" + JSON.stringify(response));
            }
        },
    });
}

function editUser(evt) {
    var username = evt.target.closest('[data-username]').dataset.username;
    location.href = route_path('user', {username: username});
}

function deleteUser(evt) {
    var username = evt.target.closest('[data-username]').dataset.username;
    var url = route_path('user', {username: username});
    eclairAjax({
        url: url,
        method: 'DELETE',
        success: function(response) {
            if (response.success) {
                var el = evt.target.closest('[data-username]');
                var nextel = el.nextElementSibling;
                el.parentNode.removeChild(el);
                nextel.parentNode.removeChild(nextel);
            }
        },
    });
}

function deleteUserRole(evt) {
    var username = evt.target.closest('[data-username]').dataset.username;
    var rolename = evt.target.closest('[data-delete-role]').dataset.deleteRole;
    eclairAjax({
        url: route_path('user_role'),
        method: 'DELETE',
        data: JSON.stringify({username: username, rolename: rolename}),
        success: function (response) {
            if (response.success) {
                var el = evt.target.closest('.tag');
                el.parentNode.removeChild(el);
            } else {
                console.error(response);
            }
        },
    });
}

document.querySelector('#create-user button').addEventListener('click', createNewUser);
document.querySelector('#create-user input').addEventListener('keypress', function(evt) {
    if (evt.key == 'Enter') {
        createNewUser();
    }
});
document.querySelectorAll('.btn-edit-user').forEach(function (btn) {
    btn.addEventListener('click', editUser);
});
document.querySelectorAll('.btn-delete-user').forEach(function (btn) {
    btn.addEventListener('click', deleteUser);
});
document.querySelectorAll('[data-delete-role]').forEach(function (btn) {
    btn.addEventListener('click', deleteUserRole);
});
