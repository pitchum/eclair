
var _ajax_lock = undefined;
function suggest_reference() {
    if (!document.querySelector('#autogen-reference').checked) {
        return;
    }
    if (!document.querySelector('#subcategory-selector').value) {
        console.debug("Select a subcategory before.");
        return;
    }
    if (!document.querySelector('#product-name').value) {
        console.debug("Enter a product name before.", );
        return;
    }
    var selected_item = document.querySelectorAll('#subcategory-selector option')[document.querySelector('#subcategory-selector').selectedIndex];
    var data = {
        "factoryname": selected_item.dataset.factoryname,
        "subcatname": document.querySelector('#subcategory-selector').value,
        "productname": document.querySelector('#product-name').value,
    };
    eclairAjax({
        url: route_path('product.generate_ref'),
        data: JSON.stringify(data),
        success: function (response) {
            console.debug(response.candidates);
            document.querySelector('#product-reference').value = response.candidates[0];
        },
    });
}


function toggle_autogen_reference(evt) {
    document.querySelector('#product-reference').readOnly = evt.target.checked;
}

document.querySelector('#product-name').addEventListener('change', suggest_reference);
document.querySelector('#subcategory-selector').addEventListener('change', suggest_reference);

document.querySelector('#autogen-reference').addEventListener('change', toggle_autogen_reference);

// Preloading route_paths in sessionStorage.routes
// XXX dirty hack, workaround the fact that route_path() is not synchronous yet
route_path('product.generate_ref');
