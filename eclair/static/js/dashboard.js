
function refreshDashboardSuperadmin() {
    app.dbapi.fetchWorkplaces(function(response) {
        var htmlI = '';
        var htmlO = '';
        var htmlP = '';
        var htmlD = '';

        var wp;
        var orderUrl, prodUrl, delivUrl;
        for (var i = 0; i < response.workplaces.length; i++) {
            wp = response.workplaces[i];
            if (wp.factory) {
                invoiceUrl = route_path('invoices', {factoryname:wp.shortname});
                htmlI += '<a class="button" href="' + invoiceUrl + '">' + wp.shortname + '</a>';
                prodUrl = route_path('production_plan', {factoryname:wp.shortname, refdate:'tomorrow'});
                htmlP += '<a class="button" href="' + prodUrl + '">' + wp.shortname + '</a>';
                delivUrl = route_path('delivery', {factoryname:wp.shortname, refdate:'tomorrow'});
                htmlD += '<a class="button" href="' + delivUrl + '">' + wp.shortname + '</a>';
            }
            if (wp.shop) {
                orderUrl = route_path('order', {clientname:wp.shortname, refdate:'tomorrow'});
                htmlO += '<a class="button" href="' + orderUrl + '">' + wp.shortname + '</a>';
            }
        }
        document.querySelector('section#invoices div').innerHTML = htmlI;
        document.querySelector('section#orders div').innerHTML = htmlO;
        document.querySelector('section#prod div').innerHTML = htmlP;
        document.querySelector('section#delivery div').innerHTML = htmlD;
        document.querySelectorAll('section.subsection').forEach(function (section) {
            section.style.display = 'block';
        });
    }); 
}
