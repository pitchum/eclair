function toggleDisplayProductCategories() {
    var inputWorkplace = document.querySelector('#workplace-selector');
    var roleIsProducer = 'producer' == document.querySelectorAll('#role-selector option')[document.querySelector('#role-selector').selectedIndex].value ;
    var opt = inputWorkplace.querySelectorAll('option')[inputWorkplace.selectedIndex];
    if (roleIsProducer && opt.dataset.factory == 'true') {
        document.querySelector('#product-categories').classList.remove('hidden');
        var factoryname = opt.textContent;
        document.querySelectorAll('#product-categories input[type=checkbox]').forEach(function(item) {
            if (item.dataset.factoryname == factoryname) {
                item.closest('label').classList.remove('hidden');
            } else {
                item.closest('label').classList.add('hidden');
            }
        });
    } else {
        document.querySelector('#product-categories').classList.add('hidden');
    }
}
document.querySelector('#workplace-selector').addEventListener('change', toggleDisplayProductCategories);
document.querySelector('#role-selector').addEventListener('change', toggleDisplayProductCategories);
