
function deleteWorkplace(evt) {
    alert("Fonction pas encore activée.\nUne telle suppression aurait beaucoup d'effets de bords.\nTODO : afficher ici un état des lieux des dégâts avant de confirmer la suppression."); // TODO Implement confirmed deletion of a workplace
    return false;
    var shortname = evt.target.closest('[data-workplace-shortname]').dataset.workplaceShortname;
    var url = route_path('workplace', {shortname: shortname});
    eclairAjax({
        url: url,
        method: 'DELETE',
        success: function(response) {
            if (response.success) {
                var el = evt.target.closest('[data-workplace-shortname]');
                var nextel = el.nextElementSibling;
                el.parentNode.removeChild(el);
                nextel.parentNode.removeChild(nextel);
            }
        },
    });
}


function editWorkplace(evt) {
    var shortname = evt.target.closest('[data-workplace-shortname]').dataset.workplaceShortname;
    location.href = route_path('workplace', {shortname: shortname});
}


function createNewWorkplace() {
    var shortname = document.querySelector('#create-workplace input').value;
    if (!shortname || shortname.length < 3) {
        console.error("invalid shortname");
        alert("Nom court non valide !");
        return false;
    }
    var url = route_path('workplaces');
    eclairAjax({
        url: url,
        data: JSON.stringify({shortname: shortname}),
        success: function(response) {
            if (response.success) {
                window.location.href = route_path('workplace', {shortname: response.workplace.shortname});
            } else {
                console.error(response);
                alert("Le lieu n'a pas pû être créé.\n\n" + JSON.stringify(response));
            }
        },
    });
}



document.querySelector('#create-workplace button').addEventListener('click', createNewWorkplace);
document.querySelector('#create-workplace input').addEventListener('change', createNewWorkplace);

document.querySelectorAll('.btn-delete-workplace').forEach(function(btn) {
    btn.addEventListener('click', deleteWorkplace);
});
document.querySelectorAll('.btn-edit-workplace').forEach(function(btn) {
    btn.addEventListener('click', editWorkplace);
});
