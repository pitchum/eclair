

function updateFilter(evt) {
    var production_center_shortname = document.querySelector('#filter-production-center').value;
    var url = route_path('products.byfactory', {factoryname: production_center_shortname});
    location.href = url;
}

document.querySelector('#filter-production-center').addEventListener('change', updateFilter);
