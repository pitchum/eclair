
if (document.querySelector('#copy-order-from')) {
    document.querySelector('#copy-order-from').addEventListener('click', function(evt) {
        var src_date = evt.target.closest('div').querySelector('input[type=date]').value;
        var url = route_path('order.clone');
        eclairAjax({
            "url": url,
            data: JSON.stringify({
                "clientname": document.querySelector('[data-clientname]').dataset.clientname,
                "refdate_src": src_date,
                "refdate_dst": document.querySelector('[data-refdate]').dataset.refdate,
            }),
            success: function (response) {
                if (response.failure) {
                    alert(response.failure);
                }
                location.reload(); // XXX could do better
            }
        });
    });
}

if (document.querySelector('#copy-order-to')) {
    document.querySelector('#copy-order-to').addEventListener('click', function(evt) {
        var dst_date = evt.target.closest('div').querySelector('input[type=date]').value;
        var url = route_path('order.clone');
        eclairAjax({
            "url": url,
            data: JSON.stringify({
                "clientname": document.querySelector('[data-clientname]').dataset.clientname,
                "refdate_dst": dst_date,
                "refdate_src": document.querySelector('[data-refdate]').dataset.refdate,
            }),
            success: function (response) {
                if (response.failure) {
                    alert(response.failure);
                }
                location.reload(); // XXX could do better
            }
        });
    });
}
