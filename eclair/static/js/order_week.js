
// Store maxRow once to make keyboard shortcuts efficients
var maxRow = document.querySelector('#order-matrix tr:last-child input.quantity').id.split('_')[1];

function focusNextCell(direction) {
    var currentCell = document.activeElement;
    var tgtCell;
    if (currentCell.tagName != 'INPUT') {
        // No cell currently selected. Choose one arbitrarily.
        switch (direction) {
            case 'ArrowUp':
                tgtCell = document.querySelector('#order-matrix tr:last-child input.quantity');
            break;
            default:
                tgtCell = document.querySelector('#order-matrix input.quantity');
        }
    } else {
        var parts = currentCell.id.split('_');
        var row = Number(parts[1]);
        var cell = Number(parts[2]);
        switch (direction) {
            case 'ArrowLeft':
                if (cell == 1) {
                    cell = 7 // XXX hard-coded value
                } else {
                    cell--;
                }
            break;
            case 'ArrowRight':
                if (cell == 7) {
                    cell = 1 // XXX hard-coded value
                } else {
                    cell++;
                }
            break;
            case 'ArrowUp':
                if (row == 1) {
                    row = maxRow;
                } else {
                    row--;
                }
            break;
            case 'ArrowDown':
                if (row == maxRow) {
                    row = 1;
                } else {
                    row++;
                }
            break;
        }
        var newid = '#qty_' + row + '_' + cell;
        tgtCell = document.querySelector(newid);
        if (!tgtCell) {
            console.warn(newid, parts);
        }
    }
    tgtCell.select();
    
    // Scroll page if needed to make the selected row visible
    var pos = tgtCell.getBoundingClientRect();
    if (pos.bottom > (window.innerHeight - 100)) {
        window.scrollBy(0, pos.bottom - window.innerHeight + 100);
    }
    if (pos.top < 100) {
        window.scrollBy(0, pos.top - 100);
    }
}

function refdate_for_column(col_index) {
    var map = {};
    document.querySelectorAll('[data-refdate]').forEach(function (el, i) {
        map[i] = el.dataset.refdate;
    });
    return map[col_index - 1];
}

function updateQuantity(qty_holder) {
    var _clientname = document.querySelector('#order-matrix').dataset.clientname;
    var _refdate = refdate_for_column(Number(qty_holder.id.split('_')[2]));
    var _product_id = qty_holder.closest('tr').dataset.productId;
    var qty = Number(qty_holder.value);
    
    app.orderapi.enqueue({
        "clientname": _clientname,
        "refdate": _refdate,
        "product_id": _product_id,
        "input": qty_holder,
    });

}


function changeVal(increase) {
    var ele = document.activeElement;
    if (ele.tagName == 'INPUT' && ele.classList.contains('quantity')) {
        var curVal = Number(ele.value);
        if (increase) {
            ele.value = ++curVal;
        } else {
            if (curVal > 0) {
              ele.value = --curVal;
            }
        }
        updateQuantity(ele);
    }
}

function keyboardShortCut(evt) {
    if (evt.defaultPrevented) {
        return; // Do nothing if the event was already processed
    }
    if (evt.ctrlKey) {
        switch (evt.key) {
            case "ArrowDown":
            case "ArrowUp":
            case "ArrowRight":
            case "ArrowLeft":
                evt.preventDefault();
                focusNextCell(evt.key);
            break;
            case "+":
                var ele = document.activeElement;
                if (ele.tagName == 'INPUT' && ele.classList.contains('quantity')) {
                    evt.preventDefault();
                }
                changeVal(true);
            break;
            case "-":
                var ele = document.activeElement;
                if (ele.tagName == 'INPUT' && ele.classList.contains('quantity')) {
                    evt.preventDefault();
                }
                changeVal(false);
            break;
        }
    }
}

document.addEventListener('keydown', keyboardShortCut, false);
document.querySelectorAll('input.quantity').forEach(function (input) {
    input.addEventListener('change', function (evt) { updateQuantity(evt.target); });
});
