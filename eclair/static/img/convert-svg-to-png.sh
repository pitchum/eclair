#! /bin/bash

origdir=${1:-orig}
curdir=${2:-$(pwd)}
height=${3:-128}

do_convert() {
    orig=$1
    dest=$2
    simplename=$(basename "${orig%.*}") # remove extension
    output=$(readlink -f "${dest}")/${simplename}-${height}px.png
    echo "Converting ${orig} to ${output}"
    inkscape "${orig}" -h ${height} --export-png=${output}
}

cd $origdir
for candidate in *svg; do 
    do_convert "${candidate}" "$curdir" "tat"
done
echo "toto"
