/**
 * eclair.js
 */

/**
 * Basic imitation of jQuery's ajax function.
 */
function eclairAjax(options) {
    var default_options = {
        async: true,
        method: 'POST',
        headers: {"X-Requested-With": "XMLHttpRequest",},
        success: function(){},
        error: function(){},
    };
    var opt = {};
    Object.assign(opt, default_options, options);
    
    fetch(opt.url, {
            credentials: "same-origin",
            method: opt.method,
            headers: new Headers(opt.headers),
            body: opt.data,
    }).then(function (response) {
        return response.json().then(function(json) {
            return {
                'status': response.status,
                'payload': json,
            };
        });
    }).then(function (response) {
        if (response.status == 200) {
            try {
                opt.success(response.payload);
            } catch (e) {
                console.error("Error in your callback function", e);
            }
        } else if (response.status == 403) {
            if ('error' in response.payload) {
                alert(response.payload.error);
            }
            if ('redirect' in response.payload) {
                location.assign(response.payload.redirect);
            } else {
                location.reload();
            }
        } else {
            opt.error();
            return undefined;
        }
    }).catch(function(error) {
        console.error('There has been a problem with your fetch operation:', error);
        if (error.status == 429) {
            // Too Many Requests
            console.error(error.statusText);
        }
    });
}

/**
 * Proxy function for pyramid's request.route_path() function.
 * 
 * Typical usage:
 * <pre>
 * window.open(route_url('product', {id:5}));
 * </pre>
 * 
 * @param route_name
 * @param params
 * @returns
 */
function route_path(route_name, params) {
    if (!sessionStorage.routes) {
        sessionStorage.routes = JSON.stringify({});
    }
    var routes = JSON.parse(sessionStorage.routes);
    var fake_params = {};
    if (params) {
        var keys = Object.keys(params);
        var k,v;
        for (var i = 0; i < keys.length; i++) {
            k = keys[i];
            fake_params[k] = k.toUpperCase();
        }
    }
    if (!routes[route_name]) {
        eclairAjax({
            url: '/__route_path__',
            data: JSON.stringify({ route_name: route_name, params: fake_params }),
            method: 'POST',
            success: function (response) {
                routes[route_name] = response.route_path;
                sessionStorage.routes = JSON.stringify(routes);
            },
        });
    }
    var result = routes[route_name];
    if (params) {
        var k, v, fake_val;
        for (var i = 0; i < keys.length; i++) {
            k = keys[i];
            v = params[k];
            fake_val = fake_params[k];
            result = result.replace(fake_val, v);
        }
    }
    return result;
}


var app = {
    version: '0.7',
    cache: {
    },
    
    toggleDebugPanel: function() {
        var panel = document.querySelector('#debug-panel');
        if (panel.classList.contains('visible')) {
            panel.classList.remove('visible');
        } else {
            panel.classList.add('visible');
        }
    },
    
    toggleNavMenu: function(evt) {
        var panel = document.querySelector('nav#main-nav');
        if (panel.classList.contains('visible')) {
            panel.classList.remove('visible');
        } else {
            panel.classList.add('visible');
        }
        if (evt) {
            evt.stopPropagation();
        }
    },

    dbapi: {
        fetchWorkplaces: function(callback) {
            var url = route_path('workplaces');
            eclairAjax({
                url: url,
                method: 'GET',
                success: callback,
            });
        },
        addUserToProductCategory: function(categoryname, usernames, callback) {
            var url = route_path('product_category', {'categoryname': categoryname});
            var data = {'add_users': usernames};
            eclairAjax({
                url: url,
                method: 'PUT',
                data: JSON.stringify(data),
                success: callback,
            });
        },
        removeUserFromProductCategory: function(categoryname, usernames, callback) {
            var url = route_path('product_category', {'categoryname': categoryname});
            var data = {'remove_users': usernames};
            eclairAjax({
                url: url,
                method: 'PUT',
                data: JSON.stringify(data),
                success: callback,
            });
        },
        createProductSubategory: function(categoryname, categorynames, callback) {
            var url = route_path('product_category', {'categoryname': categoryname});
            var data = {'create_subcategories': categorynames};
            eclairAjax({
                url: url,
                method: 'PUT',
                data: JSON.stringify(data),
                success: callback,
            });
        },
    },
    orderapi: {
        orderline_update_queue: {}, // {clientname: {refdate: {product_id: quantity} } }
        orderline_update_lock: undefined,
        _do_batch_update: function() {
            for (var clientname in app.orderapi.orderline_update_queue) {
                if (!app.orderapi.orderline_update_queue.hasOwnProperty(clientname)) {
                    console.warn("How can it be?", clientname);
                    continue;
                }
                for (var refdate in app.orderapi.orderline_update_queue[clientname]) {
                    if (!app.orderapi.orderline_update_queue[clientname].hasOwnProperty(refdate)) {
                        console.warn("How can it be?", clientname, refdate);
                        continue;
                    }
                    if (Object.keys(app.orderapi.orderline_update_queue[clientname][refdate]).length == 0) {
                        continue;
                    }
                    var url = route_path('orderlines', {
                        "clientname": clientname,
                        "refdate": refdate,
                    });
                    var data = {};
                    for (var product_id in app.orderapi.orderline_update_queue[clientname][refdate]) {
                        if (!app.orderapi.orderline_update_queue[clientname][refdate].hasOwnProperty(product_id)) {
                            console.warn("Ignoring item", clientname, refdate, product_id);
                            continue;
                        }
                        input = app.orderapi.orderline_update_queue[clientname][refdate][product_id];
                        data[product_id] = {"quantity_ordered": input.value};
                    }
                    eclairAjax({
                        url: url,
                        data: JSON.stringify(data),
                        success: function(response) {
                            for (var product_id in response.updated_orderlines) {
                                if (response.updated_orderlines.hasOwnProperty(product_id)) {
                                    var updated_quantity = response.updated_orderlines[product_id];
                                    var input = app.orderapi.orderline_update_queue[clientname][refdate][product_id];
                                    input.classList.remove('stale');
                                    input.dataset.val = updated_quantity;
                                    input.value = updated_quantity;
                                    delete app.orderapi.orderline_update_queue[clientname][refdate][product_id];
                                } else {
                                    console.warn("How can it be?");
                                }
                            }
                        },
                    });
                }
            }
            app.orderapi.orderline_update_lock = undefined; // release update lock
        },
        enqueue: function (data) {
            if (app.orderapi.orderline_update_lock !== undefined) {
                clearTimeout(app.orderapi.orderline_update_lock);
            }
            if (!app.orderapi.orderline_update_queue.hasOwnProperty(data.clientname)) {
                app.orderapi.orderline_update_queue[data.clientname] = {};
            }
            if (!app.orderapi.orderline_update_queue[data.clientname].hasOwnProperty(data.refdate)) {
                app.orderapi.orderline_update_queue[data.clientname][data.refdate] = {};
            }
            app.orderapi.orderline_update_queue[data.clientname][data.refdate][data.product_id] = data.input;
            data.input.classList.add('stale');
            app.orderapi.orderline_update_lock = setTimeout(app.orderapi._do_batch_update, 1000);
        },
    },
}

if (document.querySelector('#nav-trigger')) {
    document.querySelector('#nav-trigger').addEventListener('click', app.toggleNavMenu);
}

window.addEventListener('click', function(evt) {
    var panel = document.querySelector('nav#main-nav');
    if (panel && panel.classList.contains('visible')) {
        app.toggleNavMenu();
    }
});

var btnPrint = document.querySelector('#btn-print');
if (btnPrint) {
    btnPrint.addEventListener('click', function (evt) { window.print(); });
}
var btnReload = document.querySelector('#btn-reload');
if (btnReload) {
    btnReload.addEventListener('click', function (evt) { location.reload(); });
}

console.debug("eclair.js successfully loaded");

// Preloading route_paths in sessionStorage.routes
// XXX dirty hack, workaround the fact that route_path() is not synchronous yet
var routes = [
        { name: 'production_plan', opts: {factoryname:'AAA', refdate:'today'}},
        { name: 'workplaces'},
        { name: 'workplace', opts: {shortname: 'new'}},
        { name: 'delivery', opts: {factoryname: 'AAA', refdate:'20170101'}},
        { name: 'users'},
        { name: 'user', opts: {username: 'new'}},
        { name: 'user_role'},
        { name: 'products.byfactory', opts: {factoryname: 'PLOP'}},
        { name: 'order', opts: {clientname: 'XXX', refdate: "20170101"}},
        { name: 'invoices', opts: {factoryname: 'XXX'}},
        { name: 'invoice',  opts: {factoryname: 'XXX', refmonth: "201801", clientname: 'XXX'}},
        { name: 'product_category', opts: {'categoryname': 'XXX'}},
        { name: 'orderlinecustomproduct', opts: {'id': 42}},
        { name: 'order.clone'},
        { name: 'orderlines', opts: {clientname: 'XXX', refdate: '20170101'}},
    ];

var cnt = 0;
function _load_missing_routes() {
    if (!sessionStorage.routes) {
        sessionStorage.routes = JSON.stringify({});
    }
    if (cnt > 100) {
        console.error('watchdog forced stopping function _load_missing_routes()');
        return;
    }
    var already_loaded = JSON.parse(sessionStorage.routes);
    for (var i = 0; i < routes.length; i++) {
        var route = routes[i];
        if (already_loaded.hasOwnProperty(route.name)) {
        } else {
            cnt++;
            try {
                route_path(route.name, route.opts);
            } catch (e) {
                //console.error(e);
            }
            setTimeout(_load_missing_routes, 300);
            break;
        }
    }
}
_load_missing_routes();
