from pyramid.config import Configurator

import pkg_resources
# Fetching version number from package.
__version__ = pkg_resources.require("eclair")[0].version


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings['app_version'] = __version__
    config = Configurator(settings=settings)
    # Custom JSON renderer
    config.add_renderer('json', enhanced_json_renderer())
    
    config.include('pyramid_tm')
    config.include('pyramid_jinja2')
    config.include('.jinjafilters')
    config.include('.models')
    config.include('.routes')
    config.include('.security')
    config.scan(ignore='eclair.util')
    return config.make_wsgi_app()


def enhanced_json_renderer():
    '''
    Returns an enhanced Pyramid JSON renderer.
    Enhanced means:

    - datetimes are automatically stringified
    '''
    from pyramid.renderers import JSON
    import datetime

    json_renderer = JSON()
    def datetime_adapter(obj, request):
        """ date format """
        return obj.isoformat()
    json_renderer.add_adapter(datetime.datetime, datetime_adapter)
    json_renderer.add_adapter(datetime.time, datetime_adapter)
    return json_renderer
