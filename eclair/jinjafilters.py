from babel import dates
from datetime import datetime


def includeme(config):
    config.commit()
    jinja2_env = config.get_jinja2_environment()
    jinja2_env.filters['price'] = format_price
    jinja2_env.filters['quantity'] = format_quantity
    jinja2_env.filters['date'] = format_date
    jinja2_env.filters['datetime'] = format_datetime
    jinja2_env.filters['translate_rolename'] = translate_rolename
    jinja2_env.filters['crop'] = crop


def format_price(price):
    if price is None:
        return ''
    if isinstance(price, str):
        price = float(price)
    return '%.2f' % price


def format_quantity(quantity):
    if quantity is None:
        return ''
    if isinstance(quantity, int) or isinstance(quantity, float) and quantity.is_integer():
        return '%d' % quantity
    else:
        return '%.1f' % quantity


def format_date(value, fmt='medium'):
    if not value:
        return ''
    if isinstance(value, str):
        if len(value) == 6:
            value = datetime.strptime(value, '%Y%m')
        elif len(value) == 8:
            value = datetime.strptime(value, '%Y%m%d')
        else:
            value = datetime.strptime(value, '%Y-%m-%d')
    if fmt in ('short', 'long', 'medium', 'full'):
        return dates.format_date(value, format=fmt, locale='fr')
    elif fmt == 'form':
        fmt = 'YYYY-MM-dd'
    elif fmt == 'month':
        fmt = 'MMM YYYY'
    return dates.format_date(value, format=fmt, locale='fr')


def format_datetime(value, fmt='medium'):
    if not value:
        return ''
    if isinstance(value, str):
        value = datetime.strptime(value, '%Y%m%d')
    if fmt in ('short', 'long', 'medium', 'full'):
        return dates.format_datetime(value, format=fmt, locale='fr')
    return dates.format_datetime(value, format=fmt, locale='fr')


def translate_rolename(rolename):
    if not rolename:
        return rolename
    _map = { # XXX hard-coded values
        'seller':   'vente',
        'carrier':  'livraison',
        'producer': 'production',
        'admin':    'gestion',
        'shop':     'boutique',
        'factory':  'hub',
    }
    return _map.get(rolename, rolename)


def crop(value, maxlen):
    if not value:
        return ''
    if len(value) <= maxlen:
        return value
    return value[0:maxlen - 3] + "..."
