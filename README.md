eclair ERP
==========

eclair is a simple Web-based ERP.

It was originally developed for a network of bakeries.
One of their products is "éclair", hence the name.


Getting Started
---------------

First install some prerequisites.
On Debian stretch the following should be enough:

    sudo apt --no-install-recommends install python3-venv\
     python3-bcrypt python3-sqlalchemy

Then you can deploy and run the app quickly:

    cd <directory containing this file>
    python3 -m venv --system-site-packages venv
    source venv/bin/activate
    python setup.py develop
    alembic --config development.ini upgrade head
    eclair-cli -c development.ini db reset-superadmin
    pserve development.ini --reload


Contributing
------------

### Developer regular workflow

On a regular basis here is what working on the project would look like:

    cd <directory containing this file>
    git stash
    git pull --rebase
    git stash pop
    source venv/bin/activate
    python setup.py develop
    alembic --config development.ini upgrade head
    pserve development.ini --reload
    # ... do some changes/testing/commits
    git push


### Altering the database schema

After you changed something in the models (added/removed columns), run
alembic to generate a new migration script:

    alembic --config development.ini revision --autogenerate \
    -m "Message summarizing your DB changes"

Before committing please review and edit as needed the migration script
produced.


Releasing
---------

To create a tarball corresponding to tag 1.0.1:

    export v=1.0.1 ;     git archive --prefix=eclair-$v/ -o /tmp/eclair-$v.tar.gz $v
